## 66e réunion du groupe de travail

**Lundi 22 août 2022 à 11h30**



Personnes présentes :

   *  Angie
   *  Cpm
   * Jeey
   * Immae


### Divers

   * CR archivés pour les réunions du début de l'année : 
       * [https://framagit.org/chatons/chatonsinfos/-/blob/master/Archives/gt-stats-2022.md](https://framagit.org/chatons/chatonsinfos/-/blob/master/Archives/gt-stats-2022.md)
       * \o/ merci \o/
       * Attention, quand on met des textes entre guillemets dans le pad, l'export Markdown pose souci (dans l'idéal, éviter)
   * atelier ChatonsInfo le lundi 25/07
       * [https://date.infini.fr/oUASJPfOlTsRTH9p](https://date.infini.fr/oUASJPfOlTsRTH9p)
       * gros lapins
       * en attente nouvelle date
   * atelier "complétez les fichiers properties de votre chaton" lors du Camp CHATONS
       * \o/
       * atelier mené samedi matin (cf. pad) - deux-trois nouvelles orga properties et des mises à jour d'Immae


### Revue de [https://stats.chatons.org/](https://stats.chatons.org/) 

   * **page statistiques** [https://stats.chatons.org/chatons-stats.xhtml](https://stats.chatons.org/chatons-stats.xhtml)
       * fédération :
           * déplacement graphique nombre de services
           * déplacement graphique distribution serveur
           * ajout graphique hyperviseur
           * ajout graphique types organisations
       * organisation :
           * déplacement graphique nombre de services
           * déplacement graphique distribution serveur
           * ajout graphique hyperviseur
           * ajout graphique types organisation
       * service :
           * déplacement graphique nombre de services
           * déplacement graphique distribution serveur
           * ajout graphique hyperviseur
       * graphique nombre de services par an
           * ne prend en compe les services que si c'est dans la période de membreof
       * TODO Cpm FAIT
   * **page properties** :
       * constat de lignes différentes entre enddate et endDate
       * TODO Cpm ne pas tenir compte de la casse FAIT
   * **page générique d'un chaton** : 
       * penser à augmenter le code html avec les informations de properties pour faciliter le futur réagencement  UI/UX
           * TODO Cpm
   * **page « Statistiques » (fédération)** :
       * ajouter un donuts sur les services de paiement
           * TODO Cpm 
   * **un jour peut-être** :
       * pouvoir cliquer sur les graphiques pour voir la liste de résultats correspondant
           * par exemple pour les types d'inscription (à un service)
       * donuts sur les pays
           * pouvoir cliquer sur les résultats du camembert pour avoir une liste des chatons par pays
   * **pages Uptimes (Federation, Organization, Services)**
       * des améliorations à faire
           * TODO Cpm visibilité autres liens
           * TODO mrflos (pour l'été) : bidouiller la page statsuptime pour utiliser les filtres par état en js datatables
       * questions de statut manuel vs statut mesuré (page organization)
           * statut manuel seulement
           * statut mesuré seulement
           * les deux
           * un seul combiné des deux
           * discussion :
               * est-ce que la version manuelle est encore utile ? pertinence du mesuré
               * se poser la question de ce que cherche l'utilisateur
               * cas des statuts manuels « en travaux » ou  « fermé »
               * le statut manuel est plus important que le statut mesuré, respecté l'expression des admins
               * ne surtout pas afficher les deux
               * étudier la conjonction
           * TODO Cpm voir pour une version « combinée » avec bulle informative
   * **Flo :**
       * peut être avoir dans résumé des moyennes sans graphes, genre du texte « sur 2020 XXX visiteurs uniques, YYY ips différentes »
           * Cpm : une notion de « tendance » ?
           * Cpm : donner exemple ?
           * TODO Flo, à réfléchir l'enrichissement de texte des graphes toujours en TODO  > GT le 20 juillet > décalé
           * TODO Flo, à réfléchir à des cadres de tendances dans « Résumé » toujours en  > GT le 20 juillet > décalé
       * changer les intitulés « Web » et « Spécifique » par « Graphes de visites web » ou plus court « Graphes Web » et « Statistiques propres aux services » ou plus court « Stats des services » ?
           * Cpm : préciser l'intention
           * Flo : expliquer les items du menu type > GT le 20 juillet (annulé cause covid...)  > décalé
           * TODO Cpm : ajouter des bulles
           * TODO Flo : tester le menu métriques auprès de personnes
               * en cours toujours en TODO
           * TODO réfléchir
       * TODO Cpm afficher les champs nom et description des métrics dans les diagrammes
   * **site statique vs site dynamique ?**
       * différence de besoin entre Libre-service.eu (métrique au jour) et le collectif CHATONS (mérique au mois)
       * supprimer les vues années, semaines et jours ?
       * supprimer les périodes 12 mois, 2020 et 2021 ?
       * site statique :
           * avantages : simplicité de déploiement (est-ce nécessaire ?)
           * inconvénients : prend de la place sur disque
       * pour site dynamique :
           * avantages : plus grande interactivité, plus de liberté fonctionnelles
           * inconvénients : nécessite l'installation d'un serveur d'application Java, du travail pour finaliser
       * avis :
           * Antoine : pas de problème au site dynamique
           * Flo : peut-être finir les fonctionnalités en cours avant de faire une version 2 dynamique avec des optimisations
   * **Test disponibilité en ipv6** :
       * [https://forum.chatons.org/t/suggestions-pour-la-version-0-6/3232](https://forum.chatons.org/t/suggestions-pour-la-version-0-6/3232)
       * est-ce une information déclarative ou à calculer ?
           * mieux si c'est calculé car plus à jour et un truc en moins à faire pour les admins
       * sujet non urgent, à approfondir
       * continuer à évaluer les besoins
       * page ipv6 pour avoir un aperçu du point de vue du collectif
   * **Vérifier la présence de trackers basic. **
       * [https://forum.chatons.org/t/suggestions-pour-la-version-0-6/3232](https://forum.chatons.org/t/suggestions-pour-la-version-0-6/3232)
       * oui, très pertinent
       * valorisation : à envisager page dédiée


### Revue générateur de fichiers métrics (statoolinfos)

   * ajout support Jitsi :
       * TODO Cpm en cours
   * ajout support Nextcloud
   * ajout support XMPP
   * metrics http
       * améliorer détection OS TODO Cpm
       * améliorer détection date TODO Cpm
   * pour info: mrflos travaille sur scrumblr et pourra faire la page stat
       * \o/
   * **App yunohost pour stats.chatons.org**
       * todo mrflos : contacter tobias a propos de l'appli yunohost chatons FAIT le 21 avril
       * ping (19/05/2022) FAIT 
       * réponse tobias
           * Salut,
           * J’ai complètement oublié de te répondre, j’ai presque rien fait, j’ai juste un début de python :
               * #! /usr/bin/python3
               * import sys
               * sys.path.insert(0, « /usr/lib/moulinette/ »)
               * import yunohost
               * yunohost.init\_logging()
               * yunohost.init\_i18n()
               * from yunohost.app import app\_info()from yunohost.app import app\_list()
               * apps = [ app[‹ id ›] for app in app\_list()[‹ apps ›] ]
           * Si vous organiser un repo, je voudrais bien le lien, je pourrais essayer de passer du temps dessus.
       * Créer un dépôt -> Angie le créé > FAIT (16/06)
           * [https://framagit.org/chatons/chatonsinfos-yunohost](https://framagit.org/chatons/chatonsinfos-yunohost)
           * trouver un animateur de ce dépôt (créer, surveiller, répondre etc..) -> voir avec Flo, Tobias ou Aleks
               * TODO CPM faire un appel sur le forum
           * TODO Angie : programmer un atelier au Camp FAIT
           * TODO Angie : debrief  atelier camp
           * idée de faire un cahier des charges (une simple liste de fonctionnalités)


### Revue des catégories

   *  ([https://stats.chatons.org/category-autres.xhtml)](https://stats.chatons.org/category-autres.xhtml))
   * FAIT Angie : ajout du logiciel Garage dans Hébergement de sites web / blogs
   * FAIT Angie : ajout du logiciel Bookstack dans Hébergement d'une ferme de wikis
   * Baikal (Bechamail) :
       * déjà présent avec un i tréma
       * soit rajouter avec i non tréma
       * soit demander au code d'ignorer les i trema ?
           * TODO Cpm 
   * Buildbot (ImmaeEu) :
       * Python-based continuous integration testing framework
       * [https://www.buildbot.net/](https://www.buildbot.net/)
       * licence OK : [https://github.com/buildbot/buildbot/commit/8cbe692145983827c5df3cd5f738cc7a387ce21a](https://github.com/buildbot/buildbot/commit/8cbe692145983827c5df3cd5f738cc7a387ce21a)
       * => Outils d'automatisation
       * TODO Angie ajouter dans la catégorie
   *  Commento (ImmaeEu) : 
       * Commento is a fast, privacy-focused commenting platform
       * [https://www.commento.io/](https://www.commento.io/)
       * licence OK : [https://gitlab.com/commento/commento/-/blob/master/LICENSE](https://gitlab.com/commento/commento/-/blob/master/LICENSE)
       * => ???
       * TODO réfléchir à une nouvelle catégorie : nom et logo
   * Converse (ImmaeEU) :
       * A free and open-source XMPP chat client in your browser
       * licence OK : [https://github.com/conversejs/converse.js/blob/master/LICENSE](https://github.com/conversejs/converse.js/blob/master/LICENSE)
       * [https://conversejs.org/](https://conversejs.org/)
       * le nom officiel semble bien être Converse
       * =>  Messagerie instantanée
       * TODO Angie le rajoutera dans la catégorie ad-hoc
   *  Davical (ImmaeEU) :
       * DAViCal is a server for calendar sharing
       * licence OK : [https://gitlab.com/davical-project/davical/-/blob/master/COPYING](https://gitlab.com/davical-project/davical/-/blob/master/COPYING)
       * [https://www.davical.org/](https://www.davical.org/)
       * TODO Angie le rajoutera dans la catégorie ad-hoc
           * catégorie Agenda et catégorie Contact
   * Diaspora* (ImmaeEU) :
       * déjà présent mais sans l'étoile
       * => rajouter avec l'étoile ?
       * TODO Cpm retirer les '*' pour la comparaison de nom de logiciel
   * Galene (ImmaeEU) :
       * actuellement présent avec un accent : galène
       * « *Galène* (or *Galene*) » d'après  [https://galene.org/](https://galene.org/) 
       * => rajouter sans accent ?
       * TODO Cpm retirer les accents pour la comparaison
   * Glowing bear (ImmaeEU) :
       * WeeChat web frontend
       * frontend => module ?
       * licence OK
       * [https://www.ejabberd.im/](https://www.ejabberd.im/)
       * => messagerie instantanée
       * TODO Angie le rajoutera dans la catégorie ad-hoc
   * Mantisbt (ImmaeEU) :
       * Mantis Bug Tracker  (logiciel ancien connu)
       * licence OK :  [https://opensource.org/licenses/gpl-license](https://opensource.org/licenses/gpl-license)
       *  [https://mantisbt.org/](https://mantisbt.org/)
       * => nouvelle catégorie : gestionnaire de tickets ?
           * Mantis, MantisBT, Redmine, Gitlab, Gitea
           * TODO Angie : créer la catégorie et ajouter la liste des logiciels associés
   * Mediagoblin (ImmaeEU) : 
       * MediaGoblin is a free software media publishing platform that anyone can run
       * licence OK : [http://www.gnu.org/licenses/agpl.html](http://www.gnu.org/licenses/agpl.html)
       * [https://mediagoblin.org/](https://mediagoblin.org/)      « alternative to  Flickr, YouTube, SoundCloud, »
       * =>  Diffusion de fichiers vidéo +  Diffusion de fichiers audio 
       * TODO Angie : à ajouter dans les 2 catégories ci-dessus + Stockage / partage d'albums photos
   * Snappymail (Bechamail) :
       * Simple, modern, lightweight \& fast web-based email client.
       * licence OK : [https://github.com/the-djmaze/snappymail/blob/master/LICENSE](https://github.com/the-djmaze/snappymail/blob/master/LICENSE)
       * [https://snappymail.eu/](https://snappymail.eu/)
       * =>  Courrier électronique ~~?~~
       * TODO Angie : à ajouter dans la catégorie


### Revue des fichiers properties de membres

   * passer en revue :
       *  [https://stats.chatons.org/chatons-crawl.xhtml](https://stats.chatons.org/chatons-crawl.xhtml)
           * plus tard
       * [https://stats.chatons.org/chatons-propertyalerts.xhtml](https://stats.chatons.org/chatons-propertyalerts.xhtml)
           * rouges : 580 495 ~~500~~ ~~498~~ ~~284~~ ~~289 ~~ ~~292~~  ~~316~~
           * jaunes :  1287 1211 ~~1208~~ ~~1209~~ ~~1220~~  ~~1217~~  ~~1220~~  ~~1226~~ 
           * communiquer un jour prochain (au moment de la prochaine release - celle avec les metrics)
   * Bechamail :
       * [https://stats.chatons.org/bechamailfr-propertycheck.xhtml](https://stats.chatons.org/bechamailfr-propertycheck.xhtml)
       * organization.memberof.*.startdate     Propriété manquante
       * organization.memberof.*.status.level     Propriété manquante
       * TODO Cpm contacter  FAIT le 26/07/2022
       * FAIT OK
   * Ethibox :
       * [https://stats.chatons.org/ethibox-propertycheck.xhtml](https://stats.chatons.org/ethibox-propertycheck.xhtml)
       * organization.memberof.*.startdate     Propriété manquante
       * organization.memberof.*.status.level     Propriété manquante
       * TODO Cpm contacter  FAIT le 26/07/2022
       * FAIT OK
   * rapprochement avec fiches chatons.org (prospective) :
       * constat qu'il y a eu une convergence de la fiche service sur chatons.org et le fichier properties de service
       * comment pourrait s'envisager une étape supplémentaire de rapprochement ?
       * niveau organisation :
           * si on complète le formulaire de la fiche structure (?) sur chatons.org alors chatons.org serait à même de générer automatiquement le fichier properties de l'organisation
               * Si on automatise à la création d'une fiche, on va se retrouver avec les fiches des candidats chatons et autres huluberlus qui créent des fiches sans jamais candidater. Il faudrait donc que la génération n'intervienne que lorsqu'on modifie la valeur du champ `Etat de la candidature` en `chaton approuvé`    +1 +1
           * avantages :
               * un plus grand nombre d'organisations suivies dans stats.chatons.org
               * une gestion automatique
           * inconvénients :
               * besoin d'étudier des cas (suppression de fiche structure, sortie du collectif, etc.)
       *  niveau service :
           * si on complète le formulaire de la fiche service sur chatons.org alors chatons.org serait à même de générer automatiquement le fichier properties de service
               * comment la génération automatique de ces fiches service.properties viendront-elles alimenter la section Subs de fiches organization.properties ?
                   * très important oui
           * avantages :
               *  un plus grand nombre de services dans stats.chatons.org
               * une gestion automatique
           * besoin d'étudier des cas (suppression de fiche service, sortie du collectif, etc.)
           * Premier pas :
               * Rassembler les infos :
                   * Le nom de l'information et où se trouve-t-elle (si ça se trouve ? et où : fiche Chatons ? Properties) ?
       * TODO recensement comparatif + table de concordance, FAIT Angie \o/
           * [https://asso.framasoft.org/nextcloud/s/3cxYfqAms6Zg6pe](https://asso.framasoft.org/nextcloud/s/3cxYfqAms6Zg6pe)
       * TODO sur les prochaines réunions : définir où intégrer chaque champ de stats.chatons qui n'est pas encore présent sur chatons.org


### Revue des tickets

   * [https://framagit.org/chatons/chatonsinfos/-/issues/1](https://framagit.org/chatons/chatonsinfos/-/issues/1)
       * Redesign des encarts au dessus des tableaux
       * prévu lorsqu'on aura toutes les informations affichées
       * statut : plus tard
   * [https://framagit.org/chatons/chatonsinfos/-/issues/2](https://framagit.org/chatons/chatonsinfos/-/issues/2)
       * Dans le tableau des services de la fiche organisation des chatons, supprimer la colonne "Organisation"
       * Cpm : ce tableau est une vue mutualisée entre plusieurs pages : organisation, services, catégorie,  logiciel ; l'information est effectivement redondante pour la page organisation, mais ça permet de conserver l'homogénéité de la vue.
       * Cpm : pour gagner de la place, possibilité de ne mettre que le logo de l'organisation et le nom en bulle
       * statut : réfléchir et sinon sera traité par la grande revue visuelle prévue un jour
   * [https://framagit.org/chatons/chatonsinfos/-/issues/4](https://framagit.org/chatons/chatonsinfos/-/issues/4)
       * Penser un nouveau fichier properties dédié aux offres non logicielles, celle d'hébergement
       * statut : priorité aux services utilisateurs donc pertinent mais plus tard


### Revue des merge requests

   * [https://framagit.org/chatons/chatonsinfos/-/merge\_requests](https://framagit.org/chatons/chatonsinfos/-/merge\_requests)
       * RAS


### Revue du forum

   * [https://forum.chatons.org/c/collectif/stats-chatons-org/83](https://forum.chatons.org/c/collectif/stats-chatons-org/83)
   * [https://forum.chatons.org/t/sondage-de-date-pour-un-atelier-stats-chatons-org/3951/2](https://forum.chatons.org/t/sondage-de-date-pour-un-atelier-stats-chatons-org/3951/2)
   * [https://forum.chatons.org/t/nouvelle-entree-host-server-distribution-pour-les-fichiers-properties-services/3399/10](https://forum.chatons.org/t/nouvelle-entree-host-server-distribution-pour-les-fichiers-properties-services/3399/10)
   * [https://forum.chatons.org/t/deux-nouvelles-proprietes-pour-vos-fichiers-properties-mettez-a-jour/3970](https://forum.chatons.org/t/deux-nouvelles-proprietes-pour-vos-fichiers-properties-mettez-a-jour/3970)


### Revue des disponibilités des services (uptimes)

   * [https://stats.chatons.org/chatons-uptimes.xhtml](https://stats.chatons.org/chatons-uptimes.xhtml)
   * Sleto > Equipe
       * rouge depuis au moins 3 semaines
       * TODO Jeey/MrFlos contacter
           * Jeey relance (05/05)
               * pas de réponse
               * Outil en fin de vie, fichier properties 
       * TODO Angie relancer FAIT (16/06)
           * à voir au camp CHATONS
   * Tchat > Colibris
       * connu, en cours de réinstallation
   * LSTU > NoMagic
       * TODO Jeey le prévenir
           * FAIT (21/07)
       * Résolu
   * Immae
       * les nouveaux services d'Immae qui n'ont pas la possibilité d'avoir des URL (comptes SSH et compte FTP)
       * Il est recommandé d'avoir du coup une page descriptive pour chacun des services
       * TODO Immae -> faire les pages de présentation de service


### Revue de l'ontologie

   * rappels :
       * l'ordre des questions à se poser : préfixe, sous-préfixe
       * on rajoute .count si le mot signifiant est le préfixe
       * métriques HTTP :
           * contexte :
               * [https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/metrics.properties](https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/metrics.properties)
               * [http://www.webalizer.org/webalizer\_help.html](http://www.webalizer.org/webalizer\_help.html)
                   * [https://web.archive.org/web/20210330151216/http://www.webalizer.org/webalizer\_help.html](https://web.archive.org/web/20210330151216/http://www.webalizer.org/webalizer\_help.html)


   * mesure du CO2 :
       * en attente résultats du groupe de travail dédié


   * host.server.distribution
       * pas très propagé
       * TODO Cpm faire un rappel forum (faire un nouveau post) ++ FAIT
           * [https://forum.chatons.org/t/nouvelle-entree-host-server-distribution-pour-les-fichiers-properties-services/3399/10](https://forum.chatons.org/t/nouvelle-entree-host-server-distribution-pour-les-fichiers-properties-services/3399/10)
       * en profiter pour  :
           * service.registration.load
           * service.install.type
           * indiquer où aller voir


   * hosts.os + host.hypervisor :
       * puisque hosts.server.distribution existe alors hosts.os est moins utile
       * donc ajout :
           * *# Nom du logiciel hyperviseur (type STRING, optionnel, ex. KVM).*
           * host.provider.hypervisor = 
       * TODO modifier CHANGELOG FAIT
       * TODO Cpm implémenter STatoolInfos  FAIT
       * TODO Cpm annonce forum FAIT
           * [https://forum.chatons.org/t/deux-nouvelles-proprietes-pour-vos-fichiers-properties-mettez-a-jour/3970](https://forum.chatons.org/t/deux-nouvelles-proprietes-pour-vos-fichiers-properties-mettez-a-jour/3970)


   * fiche properties hébergement
       * [https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/hosting.properties](https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/hosting.properties)
       * déjà 2 fichiers properties remplis !
       * projection d'intégration dans stats.chatons.org
           * besoin d'un bouton « Services dédiés » 
           * certainement renommer « Services » en « Services mutualisés » +1
           * page d'index : 
               * ajouter un nombre spécifique pour les services dédiés
               * ajouter un indicateur pour différencier
               * dans le tableau : un colonne pour les services dédiés / une pour les services mutualisés
           * page d'un membre :
               * ajouter un nombre spécifique pour les services dédiés
               * ajouter un indicateur pour différencier
           * nommage : service mutualisé vs service dédié
       * TODO intégrer dans le site stats.chatons.org
       * TODO communiquer auprès du collectif sur l'existence de cette nouvelle fiche


   * hosting.software.name/website/licence.url.name etc
       * dans le cas d'un service d'hébergement dédié d'un software (et non machine nue), manque la possibilité de rajouter les informations sur le logiciel mis à disposition (Nextcloud dédié de Framasoft). Cf remontée d'info d'Inmae (par Jeey)
           * oui il faut qu'ils soient là, à la prochaine réunion on définit les noms précis à utiliser.
           * TODO Angie proposer les noms des champs


   * Ajout du champ organization.socialnetworks.pleroma
       * ajouté au fichier modèle organization.properties
       * TODO Cpm : implémenter StatoolInfos
       * pleroma = mastodon ? condisérer à part ?


   * fiche service : champ service.website obligatoire
       * mais quand le service est la fourniture de comptes SSH (ou de DNS secondaires ou MixBackUp), il n'y a d'URL à donner et donc ça créé une erreur de remplissage
       * Solution : mettre l'URL d'une page web de présentation du service


   * prévoir de communiquer sur l'usage possible des stats


### Épilogue

Date prochaine réunion ?

   * inviter Immae
   * jeudi 11h30-13h00 : 1er septembre \o/




## 67e réunion du groupe de travail

**Jeudi 1er septembre 2022 à 11h30**



Personnes présentes :

   * Angie
   *  Cpm
   *  Jeey
   *  Immae


### Divers

   * RAS


### Revue de [https://stats.chatons.org/](https://stats.chatons.org/) 

   * **page générique d'un chaton** : 
       * penser à augmenter le code html avec les informations de properties pour faciliter le futur réagencement  UI/UX
           * TODO Cpm
   * **page « Statistiques » (fédération)** :
       * ajouter un donuts sur les services de paiement
           * TODO Cpm 
   * **un jour peut-être** :
       * pouvoir cliquer sur les graphiques pour voir la liste de résultats correspondant
           * par exemple pour les types d'inscription (à un service)
       * donuts sur les pays
           * pouvoir cliquer sur les résultats du camembert pour avoir une liste des chatons par pays
   * **pages Uptimes (Federation, Organization, Services)**
       * des améliorations à faire
           * TODO Cpm visibilité autres liens
           * TODO mrflos (pour l'été) : bidouiller la page statsuptime pour utiliser les filtres par état en js datatables
       * questions de statut manuel vs statut mesuré (page organization)
           * statut manuel seulement
           * statut mesuré seulement
           * les deux
           * un seul combiné des deux
           * discussion :
               * est-ce que la version manuelle est encore utile ? pertinence du mesuré
               * se poser la question de ce que cherche l'utilisateur
               * cas des statuts manuels « en travaux » ou  « fermé »
               * le statut manuel est plus important que le statut mesuré, respecté l'expression des admins
               * ne surtout pas afficher les deux
               * étudier la conjonction
           * TODO Cpm voir pour une version « combinée » avec bulle informative
   * **Flo :**
       * peut être avoir dans résumé des moyennes sans graphes, genre du texte « sur 2020 XXX visiteurs uniques, YYY ips différentes »
           * Cpm : une notion de « tendance » ?
           * Cpm : donner exemple ?
           * TODO Flo, à réfléchir l'enrichissement de texte des graphes toujours en TODO  > GT le 20 juillet > décalé
           * TODO Flo, à réfléchir à des cadres de tendances dans « Résumé » toujours en  > GT le 20 juillet > décalé
       * changer les intitulés « Web » et « Spécifique » par « Graphes de visites web » ou plus court « Graphes Web » et « Statistiques propres aux services » ou plus court « Stats des services » ?
           * Cpm : préciser l'intention
           * Flo : expliquer les items du menu type > GT le 20 juillet (annulé cause covid...)  > décalé
           * TODO Cpm : ajouter des bulles
           * TODO Flo : tester le menu métriques auprès de personnes
               * en cours toujours en TODO
           * TODO réfléchir
       * TODO Cpm afficher les champs nom et description des métrics dans les diagrammes
   * **site statique vs site dynamique ?**
       * différence de besoin entre Libre-service.eu (métrique au jour) et le collectif CHATONS (mérique au mois)
       * supprimer les vues années, semaines et jours ?
       * supprimer les périodes 12 mois, 2020 et 2021 ?
       * site statique :
           * avantages : simplicité de déploiement (est-ce nécessaire ?)
           * inconvénients : prend de la place sur disque
       * pour site dynamique :
           * avantages : plus grande interactivité, plus de liberté fonctionnelles
           * inconvénients : nécessite l'installation d'un serveur d'application Java, du travail pour finaliser
       * avis :
           * Antoine : pas de problème au site dynamique
           * Flo : peut-être finir les fonctionnalités en cours avant de faire une version 2 dynamique avec des optimisations
   * **Test disponibilité en ipv6** :
       * [https://forum.chatons.org/t/suggestions-pour-la-version-0-6/3232](https://forum.chatons.org/t/suggestions-pour-la-version-0-6/3232)
       * est-ce une information déclarative ou à calculer ?
           * mieux si c'est calculé car plus à jour et un truc en moins à faire pour les admins
       * sujet non urgent, à approfondir
       * continuer à évaluer les besoins
       * page ipv6 pour avoir un aperçu du point de vue du collectif
   * **Vérifier la présence de trackers basic. **
       * [https://forum.chatons.org/t/suggestions-pour-la-version-0-6/3232](https://forum.chatons.org/t/suggestions-pour-la-version-0-6/3232)
       * oui, très pertinent
       * valorisation : à envisager page dédiée


### Revue générateur de fichiers métrics (statoolinfos)

   * ajout support Jitsi :
       * TODO Cpm en cours
   * ajout support Nextcloud
   * ajout support XMPP
   * metrics http
       * améliorer détection OS TODO Cpm
       * améliorer détection date TODO Cpm
   * pour info: mrflos travaille sur scrumblr et pourra faire la page stat
       * \o/
   * **App yunohost pour stats.chatons.org**
       * todo mrflos : contacter tobias a propos de l'appli yunohost chatons FAIT le 21 avril
       * ping (19/05/2022) FAIT 
       * réponse tobias
           * Salut,
           * J’ai complètement oublié de te répondre, j’ai presque rien fait, j’ai juste un début de python :
               * #! /usr/bin/python3
               * import sys
               * sys.path.insert(0, « /usr/lib/moulinette/ »)
               * import yunohost
               * yunohost.init\_logging()
               * yunohost.init\_i18n()
               * from yunohost.app import app\_info()from yunohost.app import app\_list()
               * apps = [ app[‹ id ›] for app in app\_list()[‹ apps ›] ]
           * Si vous organiser un repo, je voudrais bien le lien, je pourrais essayer de passer du temps dessus.
       * Créer un dépôt -> Angie le créé > FAIT (16/06)
           * [https://framagit.org/chatons/chatonsinfos-yunohost](https://framagit.org/chatons/chatonsinfos-yunohost)
           * trouver un animateur de ce dépôt (créer, surveiller, répondre etc..) -> voir avec Flo, Tobias ou Aleks
               * TODO CPM faire un appel sur le forum
           * TODO Angie : debrief  atelier camp
               * après relecture des CR d'ateliers, il s'avère que la question n'a pas été traitée lors du camp CHATONS. Relancer Flo, Tobias et Aleks ?
                   * OUI, il ne faut pas abandonner l'idée et cette liste de contact
                   * avec un « petit » cahier des charges, le futur contact sera plus efficace
                       * TODO CPM : rédiger un premier jet sur le pad proposé ci-dessous
           * idée de faire un cahier des charges (une simple liste de fonctionnalités)
               * pad à compléter : [https://mypads.framapad.org/p/cc-app-yunohost-stats-chatons-e4ikv7pj](https://mypads.framapad.org/p/cc-app-yunohost-stats-chatons-e4ikv7pj)


### Revue des catégories

   * références :
       * [https://stats.chatons.org/category-autres.xhtml](https://stats.chatons.org/category-autres.xhtml)
       * [https://framagit.org/chatons/chatonsinfos/-/blob/master/StatoolInfos/categories/categories.properties](https://framagit.org/chatons/chatonsinfos/-/blob/master/StatoolInfos/categories/categories.properties)
   * Baikal (Bechamail) :
       * déjà présent avec un i tréma
       * soit rajouter avec i non tréma
       * soit demander au code d'ignorer les i trema ?
           * TODO Cpm 
   * Buildbot (ImmaeEu) :
       * Python-based continuous integration testing framework
       * [https://www.buildbot.net/](https://www.buildbot.net/)
       * licence OK : [https://github.com/buildbot/buildbot/commit/8cbe692145983827c5df3cd5f738cc7a387ce21a](https://github.com/buildbot/buildbot/commit/8cbe692145983827c5df3cd5f738cc7a387ce21a)
       * => Outils d'automatisation
       * TODO Angie ajouter dans la catégorie FAIT
   *  Commento (ImmaeEu) : 
       * Commento is a fast, privacy-focused commenting platform
       * [https://www.commento.io/](https://www.commento.io/)
       * licence OK : [https://gitlab.com/commento/commento/-/blob/master/LICENSE](https://gitlab.com/commento/commento/-/blob/master/LICENSE)
       * alternative à Disqus, [https://disqus.com/](https://disqus.com/)
           * wikipédia : blog comment hosting service
       * exemple d'usage de Commento : [https://www.immae.eu/blog/2014/05/10/shell-invocation/](https://www.immae.eu/blog/2014/05/10/shell-invocation/)
       * TODO réfléchir à une nouvelle catégorie : 
           * nom : ~~gestionnaire de commentaires~~ vs plateforme de commentaires
           * ##Hébergement d'une plateforme de commentaires (alternative à Disqus)
           * categories.commenting.name=Hébergement d'une plateforme de commentaires
           * categories.commenting.description=
           * categories.commenting.logo=commenting.svg
           * categories.commenting.softwares=Commento
           * TODO Angie : ajouter au fichier categories.properties + créer le logo
   * Converse (ImmaeEU) :
       * A free and open-source XMPP chat client in your browser
       * licence OK : [https://github.com/conversejs/converse.js/blob/master/LICENSE](https://github.com/conversejs/converse.js/blob/master/LICENSE)
       * [https://conversejs.org/](https://conversejs.org/)
       * le nom officiel semble bien être Converse
       * =>  Messagerie instantanée
       * TODO Angie le rajoutera dans la catégorie ad-hoc FAIT (catégorie "chat")
   *  Davical (ImmaeEU) :
       * DAViCal is a server for calendar sharing
       * licence OK : [https://gitlab.com/davical-project/davical/-/blob/master/COPYING](https://gitlab.com/davical-project/davical/-/blob/master/COPYING)
       * [https://www.davical.org/](https://www.davical.org/)
       * TODO Angie le rajoutera dans la catégorie ad-hoc FAIT
           * catégorie Agenda et catégorie Contact
   * Diaspora* (ImmaeEU) :
       * déjà présent mais sans l'étoile
       * => rajouter avec l'étoile ?
       * TODO Cpm retirer les '*' pour la comparaison de nom de logiciel
   * Galene (ImmaeEU) :
       * actuellement présent avec un accent : galène
       * « *Galène* (or *Galene*) » d'après  [https://galene.org/](https://galene.org/) 
       * => rajouter sans accent ?
       * TODO Cpm retirer les accents pour la comparaison
   * Glowing bear (ImmaeEU) :
       * WeeChat web frontend
       * frontend => module ?
       * licence OK
       * [https://www.ejabberd.im/](https://www.ejabberd.im/)
       * => chat
       * TODO Angie le rajoutera dans la catégorie ad-hoc FAIT
   * Mantisbt (ImmaeEU) :
       * Mantis Bug Tracker  (logiciel ancien connu)
       * licence OK :  [https://opensource.org/licenses/gpl-license](https://opensource.org/licenses/gpl-license)
       *  [https://mantisbt.org/](https://mantisbt.org/)
       * => nouvelle catégorie : gestionnaire de tickets ?
           * Mantis, MantisBT, Redmine, Gitlab, Gitea
           * TODO Angie : créer la catégorie et ajouter la liste des logiciels associés FAIT
               * *#Gestionnaire de tickets pour le support technique*
               * categories.ticketing.name=Gestionnaire de tickets pour le support technique
               * categories.ticketing.description=
               * categories.ticketing.logo=ticketing.svg
               * categories.ticketing.softwares=Mantis, MantisBT, Redmine, Gitlab, Gitea, RT, Request Tracker, OSTicket, Freescout
   * Mediagoblin (ImmaeEU) : 
       * MediaGoblin is a free software media publishing platform that anyone can run
       * licence OK : [http://www.gnu.org/licenses/agpl.html](http://www.gnu.org/licenses/agpl.html)
       * [https://mediagoblin.org/](https://mediagoblin.org/)      « alternative to  Flickr, YouTube, SoundCloud, »
       * =>  Diffusion de fichiers vidéo +  Diffusion de fichiers audio 
       * TODO Angie : à ajouter dans les 2 catégories ci-dessus + Stockage / partage d'albums photos FAIT
   * Snappymail (Bechamail) :
       * Simple, modern, lightweight \& fast web-based email client.
       * licence OK : [https://github.com/the-djmaze/snappymail/blob/master/LICENSE](https://github.com/the-djmaze/snappymail/blob/master/LICENSE)
       * [https://snappymail.eu/](https://snappymail.eu/)
       * =>  Courrier électronique ~~?~~
       * TODO Angie : à ajouter dans la catégorie FAIT
   * ApacheHttpd + Nginx + Twins
       * licence OK
       * TODO Angie : ajouter à la catégorie #Hébergement de sites web / blogs / capsules FAIT
   * bind9
       * service de DNS secondaire (réplication de serveurs DNS)
       * famille : ##Outils d'administration système
       * catégorie : Outils de DNS - FAIT
           * categories.dns.name=
           * categories.dns.description= 
           * categories.dns.logo=dns.svg
           * categories.dns.softwares=bind9
   * BitlBee
       * bridge IRC vers XMPP/Matrix/Discord/Facebook
       * licence OK : a priori oui
       * TODO Angie : ajouter dans les catégories Messagerie instantanée et Chat FAIT
   * Codenames Green
       * jeu 
       * licence OK : a priori oui
       * TODO Angie : ajouter à la catégorie jeu FAIT
   * Coturn
       * serveur de relai de flux vidéo
       * licence OK :
       * categorie : proxy
       * TODO Angie ajouter + étendre la description avec « relai » FAIT
   * InfCloud
       * client web pour le logiciel dav
       * licence OK : a priori oui
       * TODO Angie : ajouter dans catégories agenda et contact FAIT
   * MariaDB
       * licence OK : oui
       * TODO Angie : créer la catégorie #Résilience - outils de réplication et de back up FAIT
           * categories.resilience.name=
           * categories.resilience.description= 
           * categories.resilience.logo=resilience.svg
           * categories.resilience.softwares=MariaDB + PostgreSQL + rsync
           * [https://c8.alamy.com/comp/P2D459/resilience-vector-icon-isolated-on-transparent-background-resilience-logo-concept-P2D459.jpg](https://c8.alamy.com/comp/P2D459/resilience-vector-icon-isolated-on-transparent-background-resilience-logo-concept-P2D459.jpg)
   * naemon
       * outil de monitoring
       * catégorie : ajouter à monitoring TODO Angie FAIT
   * Openssh 
       * pas réellement un service ? à traiter la prochaine fois
   * Paste
       * ne peut pas entrer dans la catégorie #Transfert de messages chiffrés (alternative à PasteBin) car n'est pas chiffré
       * licence OK
       * décision d'étendre la desceription de la catégorie bin TODO Angie
           * FAIT : Transfert de messages ou de bouts de texte
           * TODO : trouver un autre logo (ciseaux / logo snippet)
   * ProFTPD
       * licence OK
       * TODO Angie ajouter à la catégorie : partage de document FAIT
   * Taskwarrior
       * gestionnaire de tâches
       * licence OK : a priori oui
       * catégorie : gestionnaire de tâches TODO Angie FAIT
   * Terraforming Mars
       * jeu
       * licence OK
       * catégorie : jeux TODO Angie FAIT
   * tinc
       * comme OpenVPN
       * ajouter dans la catégorie VPN TODO Angie FAIT




### Revue des fichiers properties de membres

   * passer en revue :
       *  [https://stats.chatons.org/chatons-crawl.xhtml](https://stats.chatons.org/chatons-crawl.xhtml)
           * plus tard
       * [https://stats.chatons.org/chatons-propertyalerts.xhtml](https://stats.chatons.org/chatons-propertyalerts.xhtml)
           * rouges : 586 ~~580~~ ~~495~~ ~~500~~ ~~498~~ ~~284~~ ~~289 ~~ ~~292~~  ~~316~~
           * jaunes :  1292 ~~1287~~ ~~1211~~ ~~1208~~ ~~1209~~ ~~1220~~  ~~1217~~  ~~1220~~  ~~1226~~ 
           * communiquer un jour prochain (au moment de la prochaine release - celle avec les metrics)
   * rapprochement avec fiches chatons.org (prospective) :
       * constat qu'il y a eu une convergence de la fiche service sur chatons.org et le fichier properties de service
       * comment pourrait s'envisager une étape supplémentaire de rapprochement ?
       * niveau organisation :
           * si on complète le formulaire de la fiche structure (?) sur chatons.org alors chatons.org serait à même de générer automatiquement le fichier properties de l'organisation
               * Si on automatise à la création d'une fiche, on va se retrouver avec les fiches des candidats chatons et autres huluberlus qui créent des fiches sans jamais candidater. Il faudrait donc que la génération n'intervienne que lorsqu'on modifie la valeur du champ `Etat de la candidature` en `chaton approuvé`    +1 +1
           * avantages :
               * un plus grand nombre d'organisations suivies dans stats.chatons.org
               * une gestion automatique
           * inconvénients :
               * besoin d'étudier des cas (suppression de fiche structure, sortie du collectif, etc.)
       *  niveau service :
           * si on complète le formulaire de la fiche service sur chatons.org alors chatons.org serait à même de générer automatiquement le fichier properties de service
               * comment la génération automatique de ces fiches service.properties viendront-elles alimenter la section Subs de fiches organization.properties ?
                   * très important oui
           * avantages :
               *  un plus grand nombre de services dans stats.chatons.org
               * une gestion automatique
           * besoin d'étudier des cas (suppression de fiche service, sortie du collectif, etc.)
           * Premier pas :
               * Rassembler les infos :
                   * Le nom de l'information et où se trouve-t-elle (si ça se trouve ? et où : fiche Chatons ? Properties) ?
       * TODO recensement comparatif + table de concordance, FAIT Angie \o/
           * [https://asso.framasoft.org/nextcloud/s/3cxYfqAms6Zg6pe](https://asso.framasoft.org/nextcloud/s/3cxYfqAms6Zg6pe)
       * TODO sur les prochaines réunions : définir où intégrer chaque champ de stats.chatons qui n'est pas encore présent sur chatons.org


### Revue des tickets

   * [https://framagit.org/chatons/chatonsinfos/-/issues/1](https://framagit.org/chatons/chatonsinfos/-/issues/1)
       * Redesign des encarts au dessus des tableaux
       * prévu lorsqu'on aura toutes les informations affichées
       * statut : plus tard
   * [https://framagit.org/chatons/chatonsinfos/-/issues/2](https://framagit.org/chatons/chatonsinfos/-/issues/2)
       * Dans le tableau des services de la fiche organisation des chatons, supprimer la colonne "Organisation"
       * Cpm : ce tableau est une vue mutualisée entre plusieurs pages : organisation, services, catégorie,  logiciel ; l'information est effectivement redondante pour la page organisation, mais ça permet de conserver l'homogénéité de la vue.
       * Cpm : pour gagner de la place, possibilité de ne mettre que le logo de l'organisation et le nom en bulle
       * statut : réfléchir et sinon sera traité par la grande revue visuelle prévue un jour
   * [https://framagit.org/chatons/chatonsinfos/-/issues/4](https://framagit.org/chatons/chatonsinfos/-/issues/4)
       * Penser un nouveau fichier properties dédié aux offres non logicielles, celle d'hébergement
       * statut : priorité aux services utilisateurs donc pertinent mais plus tard


### Revue des merge requests

   * [https://framagit.org/chatons/chatonsinfos/-/merge\_requests](https://framagit.org/chatons/chatonsinfos/-/merge\_requests)
       * RAS


### Revue du forum

   * [https://forum.chatons.org/c/collectif/stats-chatons-org/83](https://forum.chatons.org/c/collectif/stats-chatons-org/83)
   * [https://forum.chatons.org/t/clarification-de-la-specification/4054](https://forum.chatons.org/t/clarification-de-la-specification/4054)
       * Clarification de la spécification
       * service.registration : Free vs Member vs Client
       * répondre TODO Cpm FAIT.


### Revue des disponibilités des services (uptimes)

   * [https://stats.chatons.org/chatons-uptimes.xhtml](https://stats.chatons.org/chatons-uptimes.xhtml)
   * Sleto > Equipe
       * rouge depuis au moins 3 semaines
       * TODO Jeey/MrFlos contacter
           * Jeey relance (05/05)
               * pas de réponse
               * Outil en fin de vie, fichier properties 
       * TODO Angie relancer FAIT (16/06)
           * à voir au camp CHATONS : pas pensé mais c'est vert depuis ce matin 🎉
   * Tchat > Colibris
       * connu, en cours de réinstallation
   * Immae
       * les nouveaux services d'Immae qui n'ont pas la possibilité d'avoir des URL (comptes SSH et compte FTP)
       * Il est recommandé d'avoir du coup une page descriptive pour chacun des services
       * TODO Immae -> faire les pages de présentation de service
   * Chalec > Pad
       * depuis hier seulement
       * TODO Cpm va gérer


### Revue de l'ontologie

   * rappels :
       * l'ordre des questions à se poser : préfixe, sous-préfixe
       * on rajout .count si le mot signifiant est le préfixe
       * métriques HTTP :
           * contexte :
               * [https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/metrics.properties](https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/metrics.properties)
               * [http://www.webalizer.org/webalizer\_help.html](http://www.webalizer.org/webalizer\_help.html)
                   * [https://web.archive.org/web/20210330151216/http://www.webalizer.org/webalizer\_help.html](https://web.archive.org/web/20210330151216/http://www.webalizer.org/webalizer\_help.html)


   * mesure du CO2 :
       * en attente résultats du groupe de travail dédié


   * fiche properties hébergement
       * [https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/hosting.properties](https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/hosting.properties)
       * déjà 2 fichiers properties remplis !
       * projection d'intégration dans stats.chatons.org
           * besoin d'un bouton « Services dédiés » 
           * certainement renommer « Services » en « Services mutualisés » +1
           * page d'index : 
               * ajouter un nombre spécifique pour les services dédiés
               * ajouter un indicateur pour différencier
               * dans le tableau : un colonne pour les services dédiés / une pour les services mutualisés
           * page d'un membre :
               * ajouter un nombre spécifique pour les services dédiés
               * ajouter un indicateur pour différencier
           * nommage : service mutualisé vs service dédié
       * TODO intégrer dans le site stats.chatons.org
       * TODO communiquer auprès du collectif sur l'existence de cette nouvelle fiche


   * hosting.software.name/website/licence.url.name etc
       * dans le cas d'un service d'hébergement dédié d'un software (et non machine nue), manque la possibilité de rajouter les informations sur le logiciel mis à disposition (Nextcloud dédié de Framasoft). Cf remontée d'info d'Inmae (par Jeey)
           * oui il faut qu'ils soient là, à la prochaine réunion on définit les noms précis à utiliser.
           * TODO Angie proposer les noms des champs


   * Ajout du champ organization.socialnetworks.pleroma
       * ajouté au fichier modèle organization.properties
       * TODO Cpm : implémenter StatoolInfos
       * pleroma = mastodon ? condisérer à part ?


   * fiche service : champ service.website obligatoire
       * mais quand le service est la fourniture de comptes SSH (ou de DNS secondaires ou MixBackUp), il n'y a d'URL à donner et donc ça créé une erreur de remplissage
       * Solution : mettre l'URL d'une page web de présentation du service
       * TODO : ajouter un commentaire dans les modèles


   * prévoir de communiquer sur l'usage possible des stats


### Épilogue

Date prochaine réunion ?

   * inviter Immae
   * jeudi 11h15-13h00
   * le 22/09




## 68e réunion du groupe de travail

**Jeudi 13 octobre 2022 à 11h15**



Personnes présentes :

   * Cpm
   * Angie
   * MrFlos


### Divers

   * RAS


### Revue de [https://stats.chatons.org/](https://stats.chatons.org/) 

   * **page générique d'un chaton** : 
       * penser à augmenter le code html avec les informations de properties pour faciliter le futur réagencement  UI/UX
           * TODO Cpm
   * **page « Statistiques » (fédération)** :
       * ajouter un donuts sur les services de paiement
           * TODO Cpm 
   * **un jour peut-être** :
       * pouvoir cliquer sur les graphiques pour voir la liste de résultats correspondant
           * par exemple pour les types d'inscription (à un service)
       * donuts sur les pays
           * pouvoir cliquer sur les résultats du camembert pour avoir une liste des chatons par pays
   * **pages Uptimes (Federation, Organization, Services)**
       * des améliorations à faire
           * TODO Cpm visibilité autres liens
           * TODO mrflos (pour l'été) : bidouiller la page statsuptime pour utiliser les filtres par état en js datatables
       * questions de statut manuel vs statut mesuré (page organization)
           * statut manuel seulement
           * statut mesuré seulement
           * les deux
           * un seul combiné des deux
           * discussion :
               * est-ce que la version manuelle est encore utile ? pertinence du mesuré
               * se poser la question de ce que cherche l'utilisateur
               * cas des statuts manuels « en travaux » ou  « fermé »
               * le statut manuel est plus important que le statut mesuré, respecté l'expression des admins
               * ne surtout pas afficher les deux
               * étudier la conjonction
           * TODO Cpm voir pour une version « combinée » avec bulle informative
   * **Flo :**
       * peut être avoir dans résumé des moyennes sans graphes, genre du texte « sur 2020 XXX visiteurs uniques, YYY ips différentes »
           * Cpm : une notion de « tendance » ?
           * Cpm : donner exemple ?
           * TODO Flo, à réfléchir l'enrichissement de texte des graphes toujours en TODO  > GT le 20 juillet > décalé
           * TODO Flo, à réfléchir à des cadres de tendances dans « Résumé » toujours en  > GT le 20 juillet > décalé
       * changer les intitulés « Web » et « Spécifique » par « Graphes de visites web » ou plus court « Graphes Web » et « Statistiques propres aux services » ou plus court « Stats des services » ?
           * Cpm : préciser l'intention
           * Flo : expliquer les items du menu type > GT le 20 juillet (annulé cause covid...)  > décalé
           * TODO Cpm : ajouter des bulles
           * TODO Flo : tester le menu métriques auprès de personnes
               * en cours toujours en TODO
           * TODO réfléchir
       * TODO Cpm afficher les champs nom et description des métrics dans les diagrammes
   * **site statique vs site dynamique ?**
       * différence de besoin entre Libre-service.eu (métrique au jour) et le collectif CHATONS (mérique au mois)
       * supprimer les vues années, semaines et jours ?
       * supprimer les périodes 12 mois, 2020 et 2021 ?
       * site statique :
           * avantages : simplicité de déploiement (est-ce nécessaire ?)
           * inconvénients : prend de la place sur disque
       * pour site dynamique :
           * avantages : plus grande interactivité, plus de liberté fonctionnelles
           * inconvénients : nécessite l'installation d'un serveur d'application Java, du travail pour finaliser
       * avis :
           * Antoine : pas de problème au site dynamique
           * Flo : peut-être finir les fonctionnalités en cours avant de faire une version 2 dynamique avec des optimisations
   * **Test disponibilité en ipv6** :
       * [https://forum.chatons.org/t/suggestions-pour-la-version-0-6/3232](https://forum.chatons.org/t/suggestions-pour-la-version-0-6/3232)
       * est-ce une information déclarative ou à calculer ?
           * mieux si c'est calculé car plus à jour et un truc en moins à faire pour les admins
       * sujet non urgent, à approfondir
       * continuer à évaluer les besoins
       * page ipv6 pour avoir un aperçu du point de vue du collectif
   * **Vérifier la présence de trackers basic. **
       * [https://forum.chatons.org/t/suggestions-pour-la-version-0-6/3232](https://forum.chatons.org/t/suggestions-pour-la-version-0-6/3232)
       * oui, très pertinent
       * valorisation : à envisager page dédiée


### Revue générateur de fichiers métrics (statoolinfos)

   * ajout support Jitsi :
       * TODO Cpm en cours
   * ajout support Nextcloud
   * ajout support XMPP
   * metrics http
       * améliorer détection OS TODO Cpm
       * améliorer détection date TODO Cpm
   * pour info: mrflos travaille sur scrumblr et pourra faire la page stat
       * \o/
   * **App yunohost pour stats.chatons.org**
       * todo mrflos : contacter tobias a propos de l'appli yunohost chatons FAIT le 21 avril
       * ping (19/05/2022) FAIT 
       * réponse tobias
           * Salut,
           * J’ai complètement oublié de te répondre, j’ai presque rien fait, j’ai juste un début de python :
               * #! /usr/bin/python3
               * import sys
               * sys.path.insert(0, « /usr/lib/moulinette/ »)
               * import yunohost
               * yunohost.init\_logging()
               * yunohost.init\_i18n()
               * from yunohost.app import app\_info()from yunohost.app import app\_list()
               * apps = [ app[‹ id ›] for app in app\_list()[‹ apps ›] ]
           * Si vous organiser un repo, je voudrais bien le lien, je pourrais essayer de passer du temps dessus.
       * Créer un dépôt -> Angie le créé > FAIT (16/06)
           * [https://framagit.org/chatons/chatonsinfos-yunohost](https://framagit.org/chatons/chatonsinfos-yunohost)
           * trouver un animateur de ce dépôt (créer, surveiller, répondre etc..) -> voir avec Flo, Tobias ou Aleks
               * TODO CPM faire un appel sur le forum
           * TODO Angie : debrief  atelier camp
               * après relecture des CR d'ateliers, il s'avère que la question n'a pas été traitée lors du camp CHATONS. Relancer Flo, Tobias et Aleks ?
                   * OUI, il ne faut pas abandonner l'idée et cette liste de contact
                   * avec un « petit » cahier des charges, le futur contact sera plus efficace
                       * TODO CPM : rédiger un premier jet sur le pad proposé ci-dessous
           * idée de faire un cahier des charges (une simple liste de fonctionnalités)
               * pad à compléter : [https://mypads.framapad.org/p/cc-app-yunohost-stats-chatons-e4ikv7pj](https://mypads.framapad.org/p/cc-app-yunohost-stats-chatons-e4ikv7pj)


### Revue des catégories

   * références :
       * [https://stats.chatons.org/category-autres.xhtml](https://stats.chatons.org/category-autres.xhtml)
       * [https://framagit.org/chatons/chatonsinfos/-/blob/master/StatoolInfos/categories/categories.properties](https://framagit.org/chatons/chatonsinfos/-/blob/master/StatoolInfos/categories/categories.properties)
   * Baikal (Bechamail) :
       * déjà présent avec un i tréma
       * soit rajouter avec i non tréma
       * soit demander au code d'ignorer les i trema ?
           * TODO Cpm 
   * Diaspora* (ImmaeEU) :
       * déjà présent mais sans l'étoile
       * => rajouter avec l'étoile ?
       * TODO Cpm retirer les '*' pour la comparaison de nom de logiciel
   * Galene (ImmaeEU) :
       * actuellement présent avec un accent : galène
       * « *Galène* (or *Galene*) » d'après  [https://galene.org/](https://galene.org/) 
       * => rajouter sans accent ?
       * TODO Cpm retirer les accents pour la comparaison
   * Openssh 
       * pas réellement un service ? à traiter la prochaine fois
       * openssh / ProFTPD sont des implémentations de protocoles, pas vraiment des services
       * TODO Angie : créer de nouvelles catégories
           * Fournisseur de machines physiques ou virtuelles
               * Openssh
           * Fournisseur de stockage (accès un support de stockage, sans faire tourner des applications)
               * Minio, BorgBackup, Borgmatic, Garage, ProFTPD, rsync
           * Catégorie "Outils de réplication et de backup" -> recoupements à faire pour rsync
           * Fournisseur de bases de données
               * MariaDB, PostgreSQL, MongoDB, SQLite, redis, couchDb, cassandra




### Revue des fichiers properties de membres

   * passer en revue :
       *  [https://stats.chatons.org/chatons-crawl.xhtml](https://stats.chatons.org/chatons-crawl.xhtml)
           * plus tard
       * [https://stats.chatons.org/chatons-propertyalerts.xhtml](https://stats.chatons.org/chatons-propertyalerts.xhtml)
           * rouges : 581 ~~586~~ ~~580~~ ~~495~~ ~~500~~ ~~498~~ ~~284~~ ~~289 ~~ ~~292~~  ~~316~~
           * jaunes :  1273 ~~1292~~ ~~1287~~ ~~1211~~ ~~1208~~ ~~1209~~ ~~1220~~  ~~1217~~  ~~1220~~  ~~1226~~ 
           * communiquer un jour prochain (au moment de la prochaine release - celle avec les metrics)
   * rapprochement avec fiches chatons.org (prospective) :
       * constat qu'il y a eu une convergence de la fiche service sur chatons.org et le fichier properties de service
       * comment pourrait s'envisager une étape supplémentaire de rapprochement ?
       * niveau organisation :
           * si on complète le formulaire de la fiche structure (?) sur chatons.org alors chatons.org serait à même de générer automatiquement le fichier properties de l'organisation
               * Si on automatise à la création d'une fiche, on va se retrouver avec les fiches des candidats chatons et autres huluberlus qui créent des fiches sans jamais candidater. Il faudrait donc que la génération n'intervienne que lorsqu'on modifie la valeur du champ `Etat de la candidature` en `chaton approuvé`    +1 +1
           * avantages :
               * un plus grand nombre d'organisations suivies dans stats.chatons.org
               * une gestion automatique
           * inconvénients :
               * besoin d'étudier des cas (suppression de fiche structure, sortie du collectif, etc.)
       *  niveau service :
           * si on complète le formulaire de la fiche service sur chatons.org alors chatons.org serait à même de générer automatiquement le fichier properties de service
               * comment la génération automatique de ces fiches service.properties viendront-elles alimenter la section Subs de fiches organization.properties ?
                   * très important oui
           * avantages :
               *  un plus grand nombre de services dans stats.chatons.org
               * une gestion automatique
           * besoin d'étudier des cas (suppression de fiche service, sortie du collectif, etc.)
           * Premier pas :
               * Rassembler les infos :
                   * Le nom de l'information et où se trouve-t-elle (si ça se trouve ? et où : fiche Chatons ? Properties) ?
       * TODO recensement comparatif + table de concordance, FAIT Angie \o/
           * [https://asso.framasoft.org/nextcloud/s/3cxYfqAms6Zg6pe](https://asso.framasoft.org/nextcloud/s/3cxYfqAms6Zg6pe)
       * TODO sur les prochaines réunions : définir où intégrer chaque champ de stats.chatons qui n'est pas encore présent sur chatons.org


### Revue des tickets

   * [https://framagit.org/chatons/chatonsinfos/-/issues/1](https://framagit.org/chatons/chatonsinfos/-/issues/1)
       * Redesign des encarts au dessus des tableaux
       * prévu lorsqu'on aura toutes les informations affichées
       * statut : plus tard
   * [https://framagit.org/chatons/chatonsinfos/-/issues/2](https://framagit.org/chatons/chatonsinfos/-/issues/2)
       * Dans le tableau des services de la fiche organisation des chatons, supprimer la colonne "Organisation"
       * Cpm : ce tableau est une vue mutualisée entre plusieurs pages : organisation, services, catégorie,  logiciel ; l'information est effectivement redondante pour la page organisation, mais ça permet de conserver l'homogénéité de la vue.
       * Cpm : pour gagner de la place, possibilité de ne mettre que le logo de l'organisation et le nom en bulle
       * statut : réfléchir et sinon sera traité par la grande revue visuelle prévue un jour
   * [https://framagit.org/chatons/chatonsinfos/-/issues/4](https://framagit.org/chatons/chatonsinfos/-/issues/4)
       * Penser un nouveau fichier properties dédié aux offres non logicielles, celle d'hébergement
       * statut : priorité aux services utilisateurs donc pertinent mais plus tard


### Revue des merge requests

   * [https://framagit.org/chatons/chatonsinfos/-/merge\_requests](https://framagit.org/chatons/chatonsinfos/-/merge\_requests)
       * RAS


### Revue du forum

   * [https://forum.chatons.org/c/collectif/stats-chatons-org/83](https://forum.chatons.org/c/collectif/stats-chatons-org/83)
   * [https://forum.chatons.org/t/clarification-de-la-specification/4054](https://forum.chatons.org/t/clarification-de-la-specification/4054)
       * RAS


### Revue des disponibilités des services (uptimes)

   * [https://stats.chatons.org/chatons-uptimes.xhtml](https://stats.chatons.org/chatons-uptimes.xhtml)
   * Tchat > Colibris
       * connu, en cours de réinstallation
   * Immae
       * les nouveaux services d'Immae qui n'ont pas la possibilité d'avoir des URL (comptes SSH et compte FTP)
       * Il est recommandé d'avoir du coup une page descriptive pour chacun des services
       * TODO Immae -> faire les pages de présentation de service
   * Cloud Automario      AutoMario
       * indisponibilité intermittente
       * rapport avec changement DNS en cours ?
       * [https://stats.chatons.org/automario-cloudautomario-uptimes.xhtml](https://stats.chatons.org/automario-cloudautomario-uptimes.xhtml)
       * FAIT Angie : lui envoyer un mp sur le forum
   *   ExaCloud      Exarius
       * indisponibilité intermittente
       * à contacter la prochaine fois
   *   Tâches      Sans-nuage / ARN
       * depuis le 25/09, ~~indisponible~~ en fait très lent
       * [https://stats.chatons.org/sansnuagearn-taches-uptimes.xhtml](https://stats.chatons.org/sansnuagearn-taches-uptimes.xhtml)
       * TODO Mrflos contacter fait


### Revue de l'ontologie

   * rappels :
       * l'ordre des questions à se poser : préfixe, sous-préfixe
       * on rajout .count si le mot signifiant est le préfixe
       * métriques HTTP :
           * contexte :
               * [https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/metrics.properties](https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/metrics.properties)
               * [http://www.webalizer.org/webalizer\_help.html](http://www.webalizer.org/webalizer\_help.html)
                   * [https://web.archive.org/web/20210330151216/http://www.webalizer.org/webalizer\_help.html](https://web.archive.org/web/20210330151216/http://www.webalizer.org/webalizer\_help.html)


   * mesure du CO2 :
       * en attente résultats du groupe de travail dédié
       * suite à la réunion du GT, l'élément le plus factuel serait la mesure de la consommation électrique (kWh) de chaque service. Sinon il n'y a pas de consensus sur les mesures.
       * cf un widget à la mode chez les lownumisateurices [https://www.websitecarbon.com/website/louisderrac-com-2021-11-03-eduquer-au-numerique-daccord-mais-pas-nimporte-lequel-et-pas-nimporte-comment/](https://www.websitecarbon.com/website/louisderrac-com-2021-11-03-eduquer-au-numerique-daccord-mais-pas-nimporte-lequel-et-pas-nimporte-comment/)
       * imaginer un système de grille pour agglomérer des indicateurs pour faire un indicateur
       * kwh et co2 ne sont pas suffisants, faut d'autres infos, âge machine, durée exploitation
       * certaines informations supplémentaires seraient donc nécessaires mais pas en tant que métrique mais fichier properties service


   * fiche properties hébergement
       * [https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/hosting.properties](https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/hosting.properties)
       * déjà 2 fichiers properties remplis !
       * projection d'intégration dans stats.chatons.org
           * besoin d'un bouton « Services dédiés » 
           * certainement renommer « Services » en « Services mutualisés » +1
           * page d'index : 
               * ajouter un nombre spécifique pour les services dédiés
               * ajouter un indicateur pour différencier
               * dans le tableau : un colonne pour les services dédiés / une pour les services mutualisés
           * page d'un membre :
               * ajouter un nombre spécifique pour les services dédiés
               * ajouter un indicateur pour différencier
           * nommage : service mutualisé vs service dédié
       * TODO intégrer dans le site stats.chatons.org
       * TODO communiquer auprès du collectif sur l'existence de cette nouvelle fiche


   * hosting.software.name/website/licence.url.name etc
       * dans le cas d'un service d'hébergement dédié d'un software (et non machine nue), manque la possibilité de rajouter les informations sur le logiciel mis à disposition (Nextcloud dédié de Framasoft). Cf remontée d'info d'Inmae (par Jeey)
           * oui il faut qu'ils soient là, à la prochaine réunion on définit les noms précis à utiliser.
           * TODO Angie ajouter les noms des champs


   * Ajout du champ organization.socialnetworks.pleroma
       * ajouté au fichier modèle organization.properties
       * TODO Cpm : implémenter StatoolInfos
       * pleroma = mastodon ? condisérer à part ?


   * fiche service : champ service.website obligatoire
       * mais quand le service est la fourniture de comptes SSH (ou de DNS secondaires ou MixBackUp), il n'y a d'URL à donner et donc ça créé une erreur de remplissage
       * Solution : mettre l'URL d'une page web de présentation du service
       * TODO : ajouter un commentaire dans les modèles


   * prévoir de communiquer sur l'usage possible des stats


### Épilogue

Date prochaine réunion ?

   * inviter Immae
   * jeudi 11h15-13h00
   * le 10/10 TODO Cpm forum mp Jee + Immae FAIT






## 69e réunion du groupe de travail

**Jeudi 10 novembre 2022 à 11h15**



Personnes présentes :

   * Immae
   * Cpm
   * Angie
   * mrflos


### Divers

   * RAS


### Revue de [https://stats.chatons.org/](https://stats.chatons.org/) 

   * page catégories et logiciels
       * améliorer la comparaison des nom de logiciels (sans accent, sans majuscule, sans espace)
       * TODO Cpm FAIT
   * page catégorie :
       * [https://stats.chatons.org/category-agregateurdefluxrss.xhtml](https://stats.chatons.org/category-agregateurdefluxrss.xhtml)
       * bug : n'affiche que 5 lignes au lieu de 7
       * bug aussi sur Agenda : 29 lignes au lieu de 31
       * TODO Cpm
   * **page générique d'un chaton** : 
       * penser à augmenter le code html avec les informations de properties pour faciliter le futur réagencement  UI/UX
           * TODO Cpm
   * **page « Statistiques » (fédération)** :
       * ajouter un donuts sur les services de paiement
           * TODO Cpm 
   * **un jour peut-être** :
       * pouvoir cliquer sur les graphiques pour voir la liste de résultats correspondant
           * par exemple pour les types d'inscription (à un service)
       * donuts sur les pays
           * pouvoir cliquer sur les résultats du camembert pour avoir une liste des chatons par pays
   * **pages Uptimes (Federation, Organization, Services)**
       * des améliorations à faire
           * TODO Cpm visibilité autres liens
           * TODO mrflos (pour l'été) : bidouiller la page statsuptime pour utiliser les filtres par état en js datatables
       * questions de statut manuel vs statut mesuré (page organization)
           * statut manuel seulement
           * statut mesuré seulement
           * les deux
           * un seul combiné des deux
           * discussion :
               * est-ce que la version manuelle est encore utile ? pertinence du mesuré
               * se poser la question de ce que cherche l'utilisateur
               * cas des statuts manuels « en travaux » ou  « fermé »
               * le statut manuel est plus important que le statut mesuré, respecté l'expression des admins
               * ne surtout pas afficher les deux
               * étudier la conjonction
           * TODO Cpm voir pour une version « combinée » avec bulle informative
   * **Flo :**
       * peut être avoir dans résumé des moyennes sans graphes, genre du texte « sur 2020 XXX visiteurs uniques, YYY ips différentes »
           * Cpm : une notion de « tendance » ?
           * Cpm : donner exemple ?
           * TODO Flo, à réfléchir l'enrichissement de texte des graphes toujours en TODO  > GT le 20 juillet > décalé
           * TODO Flo, à réfléchir à des cadres de tendances dans « Résumé » toujours en  > GT le 20 juillet > décalé
       * changer les intitulés « Web » et « Spécifique » par « Graphes de visites web » ou plus court « Graphes Web » et « Statistiques propres aux services » ou plus court « Stats des services » ?
           * Cpm : préciser l'intention
           * Flo : expliquer les items du menu type > GT le 20 juillet (annulé cause covid...)  > décalé
           * TODO Cpm : ajouter des bulles
           * TODO Flo : tester le menu métriques auprès de personnes
               * en cours toujours en TODO
           * TODO réfléchir
       * TODO Cpm afficher les champs nom et description des métrics dans les diagrammes
   * **site statique vs site dynamique ?**
       * différence de besoin entre Libre-service.eu (métrique au jour) et le collectif CHATONS (mérique au mois)
       * supprimer les vues années, semaines et jours ?
       * supprimer les périodes 12 mois, 2020 et 2021 ?
       * site statique :
           * avantages : simplicité de déploiement (est-ce nécessaire ?)
           * inconvénients : prend de la place sur disque
       * pour site dynamique :
           * avantages : plus grande interactivité, plus de liberté fonctionnelles
           * inconvénients : nécessite l'installation d'un serveur d'application Java, du travail pour finaliser
       * avis :
           * Antoine : pas de problème au site dynamique
           * Flo : peut-être finir les fonctionnalités en cours avant de faire une version 2 dynamique avec des optimisations
   * **Test disponibilité en ipv6** :
       * [https://forum.chatons.org/t/suggestions-pour-la-version-0-6/3232](https://forum.chatons.org/t/suggestions-pour-la-version-0-6/3232)
       * est-ce une information déclarative ou à calculer ?
           * mieux si c'est calculé car plus à jour et un truc en moins à faire pour les admins
       * sujet non urgent, à approfondir
       * continuer à évaluer les besoins
       * page ipv6 pour avoir un aperçu du point de vue du collectif
   * **Vérifier la présence de trackers basic. **
       * [https://forum.chatons.org/t/suggestions-pour-la-version-0-6/3232](https://forum.chatons.org/t/suggestions-pour-la-version-0-6/3232)
       * oui, très pertinent
       * valorisation : à envisager page dédiée


### Revue générateur de fichiers métrics (statoolinfos)

   * ajout support Jitsi :
       * TODO Cpm en cours
   * ajout support Nextcloud
   * ajout support XMPP
   * metrics http
       * améliorer détection OS TODO Cpm
       * améliorer détection date TODO Cpm
   * pour info: mrflos travaille sur scrumblr et pourra faire la page stat
       * \o/
   * **App yunohost pour stats.chatons.org**
       * todo mrflos : contacter tobias a propos de l'appli yunohost chatons FAIT le 21 avril
       * ping (19/05/2022) FAIT 
       * réponse tobias
           * Salut,
           * J’ai complètement oublié de te répondre, j’ai presque rien fait, j’ai juste un début de python :
               * #! /usr/bin/python3
               * import sys
               * sys.path.insert(0, « /usr/lib/moulinette/ »)
               * import yunohost
               * yunohost.init\_logging()
               * yunohost.init\_i18n()
               * from yunohost.app import app\_info()from yunohost.app import app\_list()
               * apps = [ app[‹ id ›] for app in app\_list()[‹ apps ›] ]
           * Si vous organiser un repo, je voudrais bien le lien, je pourrais essayer de passer du temps dessus.
       * Créer un dépôt -> Angie le créé > FAIT (16/06)
           * [https://framagit.org/chatons/chatonsinfos-yunohost](https://framagit.org/chatons/chatonsinfos-yunohost)
           * trouver un animateur de ce dépôt (créer, surveiller, répondre etc..) -> voir avec Flo, Tobias ou Aleks
               * TODO CPM faire un appel sur le forum
           * TODO Angie : debrief  atelier camp
               * après relecture des CR d'ateliers, il s'avère que la question n'a pas été traitée lors du camp CHATONS. Relancer Flo, Tobias et Aleks ?
                   * OUI, il ne faut pas abandonner l'idée et cette liste de contact
                   * avec un « petit » cahier des charges, le futur contact sera plus efficace
                       * TODO CPM : rédiger un premier jet sur le pad proposé ci-dessous
           * idée de faire un cahier des charges (une simple liste de fonctionnalités)
               * pad à compléter : [https://mypads.framapad.org/p/cc-app-yunohost-stats-chatons-e4ikv7pj](https://mypads.framapad.org/p/cc-app-yunohost-stats-chatons-e4ikv7pj)


### Revue des catégories

   * références :
       * [https://stats.chatons.org/category-autres.xhtml](https://stats.chatons.org/category-autres.xhtml)
       * [https://framagit.org/chatons/chatonsinfos/-/blob/master/StatoolInfos/categories/categories.properties](https://framagit.org/chatons/chatonsinfos/-/blob/master/StatoolInfos/categories/categories.properties)
   * Baikal (Bechamail) :
       * déjà présent avec un i tréma
       * soit rajouter avec i non tréma
       * soit demander au code d'ignorer les i trema ?
           * TODO Cpm FAIT
   * Diaspora* (ImmaeEU) :
       * déjà présent mais sans l'étoile
       * => rajouter avec l'étoile ?
       * TODO Cpm retirer les '*' pour la comparaison de nom de logiciel FAIT
   * Galene (ImmaeEU) :
       * actuellement présent avec un accent : galène
       * « *Galène* (or *Galene*) » d'après  [https://galene.org/](https://galene.org/) 
       * => rajouter sans accent ?
       * TODO Cpm retirer les accents pour la comparaison FAIT
   * Openssh 
       * pas réellement un service ? à traiter la prochaine fois
       * openssh / ProFTPD sont des implémentations de protocoles, pas vraiment des services
       * Angie : créer de nouvelles catégories FAIT
           * *##Fournisseur de comptes Shell, machines physiques ou virtuelles*
           * categories.server.name= Fournisseur de comptes Shell, machines physiques ou virtuelles
           * categories.server.description= 
           * categories.server.logo=server.svg
           * categories.server.softwares=Open+9+SSH


           * *##Fournisseur de stockage (accès à un support de stockage, sans faire tourner des applications)*
           * categories.storage.name= Fournisseur de stockage
           * categories.storage.description= 
           * categories.storage.logo=storage.svg
           * categories.storage.softwares=MinIO, BorgBackup, borgmatic, Garage, ProFTPD, rsync


           * *##Fournisseur de bases de données*
           * categories.database.name= Fournisseur de bases de données
           * categories.database.description= 
           * categories.database.logo=database.svg
           * categories.database.softwares=MariaDB, PostgreSQL, MongoDB, SQLite, Redis, CouchDb, Cassandra
   * supprimer doublons :
       * signaturepdf
       * ApacheHttpd
       * File To Link
       * Diaspora*
       * Element-Web
       * jitsimeet
       * Open Street Map
       * rocket chat




### Revue des fichiers properties de membres

   * passer en revue :
       *  [https://stats.chatons.org/chatons-crawl.xhtml](https://stats.chatons.org/chatons-crawl.xhtml)
           * plus tard
       * [https://stats.chatons.org/chatons-propertyalerts.xhtml](https://stats.chatons.org/chatons-propertyalerts.xhtml)
           * rouges : 562 ~~581~~ ~~586~~ ~~580~~ ~~495~~ ~~500~~ ~~498~~ ~~284~~ ~~289 ~~ ~~292~~  ~~316~~
           * jaunes :  1266 ~~1273~~ ~~1292~~ ~~1287~~ ~~1211~~ ~~1208~~ ~~1209~~ ~~1220~~  ~~1217~~  ~~1220~~  ~~1226~~ 
           * communiquer un jour prochain (au moment de la prochaine release - celle avec les metrics)
   * rapprochement avec fiches chatons.org (prospective) :
       * constat qu'il y a eu une convergence de la fiche service sur chatons.org et le fichier properties de service
       * comment pourrait s'envisager une étape supplémentaire de rapprochement ?
       * niveau organisation :
           * si on complète le formulaire de la fiche structure (?) sur chatons.org alors chatons.org serait à même de générer automatiquement le fichier properties de l'organisation
               * Si on automatise à la création d'une fiche, on va se retrouver avec les fiches des candidats chatons et autres huluberlus qui créent des fiches sans jamais candidater. Il faudrait donc que la génération n'intervienne que lorsqu'on modifie la valeur du champ `Etat de la candidature` en `chaton approuvé`    +1 +1
           * avantages :
               * un plus grand nombre d'organisations suivies dans stats.chatons.org
               * une gestion automatique
           * inconvénients :
               * besoin d'étudier des cas (suppression de fiche structure, sortie du collectif, etc.)
       *  niveau service :
           * si on complète le formulaire de la fiche service sur chatons.org alors chatons.org serait à même de générer automatiquement le fichier properties de service
               * comment la génération automatique de ces fiches service.properties viendront-elles alimenter la section Subs de fiches organization.properties ?
                   * très important oui
           * avantages :
               *  un plus grand nombre de services dans stats.chatons.org
               * une gestion automatique
           * besoin d'étudier des cas (suppression de fiche service, sortie du collectif, etc.)
           * Premier pas :
               * Rassembler les infos :
                   * Le nom de l'information et où se trouve-t-elle (si ça se trouve ? et où : fiche Chatons ? Properties) ?
       * TODO recensement comparatif + table de concordance, FAIT Angie \o/
           * [https://asso.framasoft.org/nextcloud/s/3cxYfqAms6Zg6pe](https://asso.framasoft.org/nextcloud/s/3cxYfqAms6Zg6pe)
       * TODO sur les prochaines réunions : définir où intégrer chaque champ de stats.chatons qui n'est pas encore présent sur chatons.org
   * cas stemy
       * [https://forum.chatons.org/t/stop-ou-encore/4196/10](https://forum.chatons.org/t/stop-ou-encore/4196/10)
       * [https://www.chatons.org/chatons/stemyme](https://www.chatons.org/chatons/stemyme)
       * invité à maj sa fiche CHATONS FAIT mrflos
       * TODO modifier fichier proporties
   * cas automario
       * TODO Angie contact maj fichier properties  (organization.status)


### Revue des tickets

   * [https://framagit.org/chatons/chatonsinfos/-/issues/1](https://framagit.org/chatons/chatonsinfos/-/issues/1)
       * Redesign des encarts au dessus des tableaux
       * prévu lorsqu'on aura toutes les informations affichées
       * statut : plus tard
   * [https://framagit.org/chatons/chatonsinfos/-/issues/2](https://framagit.org/chatons/chatonsinfos/-/issues/2)
       * Dans le tableau des services de la fiche organisation des chatons, supprimer la colonne "Organisation"
       * Cpm : ce tableau est une vue mutualisée entre plusieurs pages : organisation, services, catégorie,  logiciel ; l'information est effectivement redondante pour la page organisation, mais ça permet de conserver l'homogénéité de la vue.
       * Cpm : pour gagner de la place, possibilité de ne mettre que le logo de l'organisation et le nom en bulle
       * statut : réfléchir et sinon sera traité par la grande revue visuelle prévue un jour
   * [https://framagit.org/chatons/chatonsinfos/-/issues/4](https://framagit.org/chatons/chatonsinfos/-/issues/4)
       * Penser un nouveau fichier properties dédié aux offres non logicielles, celle d'hébergement
       * statut : priorité aux services utilisateurs donc pertinent mais plus tard


### Revue des merge requests

   * [https://framagit.org/chatons/chatonsinfos/-/merge\_requests](https://framagit.org/chatons/chatonsinfos/-/merge\_requests)
       * RAS


### Revue du forum

   * [https://forum.chatons.org/c/collectif/stats-chatons-org/83](https://forum.chatons.org/c/collectif/stats-chatons-org/83)
   * [https://forum.chatons.org/t/clarification-de-la-specification/4054](https://forum.chatons.org/t/clarification-de-la-specification/4054)
       * RAS


### Revue des disponibilités des services (uptimes)

   * [https://stats.chatons.org/chatons-uptimes.xhtml](https://stats.chatons.org/chatons-uptimes.xhtml)
   * Tchat > Colibris
       * connu, en cours de réinstallation
   * Immae
       * les nouveaux services d'Immae qui n'ont pas la possibilité d'avoir des URL (comptes SSH et compte FTP)
       * Il est recommandé d'avoir du coup une page descriptive pour chacun des services
       * TODO Immae -> faire les pages de présentation de service
   * Chalec > Pad
       * depuis hier seulement
       * TODO Cpm va gérer
       * redevenu ok
   * Cloud Automario      AutoMario
       * indisponibilité intermittente
       * rapport avec changement DNS en cours ?
       * [https://stats.chatons.org/automario-cloudautomario-uptimes.xhtml](https://stats.chatons.org/automario-cloudautomario-uptimes.xhtml)
       * FAIT Angie : lui envoyer un mp sur le forum
       * redevenu OK
   *   ExaCloud      Exarius
       * indisponibilité intermittente
       * à contacter la prochaine fois FAIT Angie 
   *   Tâches      Sans-nuage / ARN
       * depuis le 25/09, ~~indisponible~~ en fait très lent
       * [https://stats.chatons.org/sansnuagearn-taches-uptimes.xhtml](https://stats.chatons.org/sansnuagearn-taches-uptimes.xhtml)
       * TODO Mrflos contacter fait
   * Bastet
       * agenda, mobilizon, pad avancé, transfert d'image, contacts
       * rouges depuis le 31/10
       * TODO Angie contacter Dino -> contacté
   * DevlogPro
       * pad, peertube, video
       * rouges depuis plus de 2 semaines
       * TODO Immae contacter -> contacté
           * travaux en cours
   *   Taskwarrior      ImmaeEu
       * rouge car 401  htaccess non encore géré par StatoolInfos
       * TODO Cpm
   * 



### Revue de l'ontologie

   * rappels :
       * l'ordre des questions à se poser : préfixe, sous-préfixe
       * on rajout .count si le mot signifiant est le préfixe
       * métriques HTTP :
           * contexte :
               * [https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/metrics.properties](https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/metrics.properties)
               * [http://www.webalizer.org/webalizer\_help.html](http://www.webalizer.org/webalizer\_help.html)
                   * [https://web.archive.org/web/20210330151216/http://www.webalizer.org/webalizer\_help.html](https://web.archive.org/web/20210330151216/http://www.webalizer.org/webalizer\_help.html)


   * mesure du CO2 :
       * en attente résultats du groupe de travail dédié
       * suite à la réunion du GT, l'élément le plus factuel serait la mesure de la consommation électrique (kWh) de chaque service. Sinon il n'y a pas de consensus sur les mesures.
       * cf un widget à la mode chez les lownumisateurices [https://www.websitecarbon.com/website/louisderrac-com-2021-11-03-eduquer-au-numerique-daccord-mais-pas-nimporte-lequel-et-pas-nimporte-comment/](https://www.websitecarbon.com/website/louisderrac-com-2021-11-03-eduquer-au-numerique-daccord-mais-pas-nimporte-lequel-et-pas-nimporte-comment/)
       * imaginer un système de grille pour agglomérer des indicateurs pour faire un indicateur
       * kwh et co2 ne sont pas suffisants, faut d'autres infos, âge machine, durée exploitation
       * certaines informations supplémentaires seraient donc nécessaires mais pas en tant que métrique mais fichier properties service


   * fiche properties hébergement
       * [https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/hosting.properties](https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/hosting.properties)
       * déjà 2 fichiers properties remplis !
       * projection d'intégration dans stats.chatons.org
           * besoin d'un bouton « Services dédiés » 
           * certainement renommer « Services » en « Services mutualisés » +1
           * page d'index : 
               * ajouter un nombre spécifique pour les services dédiés
               * ajouter un indicateur pour différencier
               * dans le tableau : un colonne pour les services dédiés / une pour les services mutualisés
           * page d'un membre :
               * ajouter un nombre spécifique pour les services dédiés
               * ajouter un indicateur pour différencier
           * nommage : service mutualisé vs service dédié
       * TODO intégrer dans le site stats.chatons.org
       * TODO communiquer auprès du collectif sur l'existence de cette nouvelle fiche


   * hosting.software.name/website/licence.url.name etc
       * dans le cas d'un service d'hébergement dédié d'un software (et non machine nue), manque la possibilité de rajouter les informations sur le logiciel mis à disposition (Nextcloud dédié de Framasoft). Cf remontée d'info d'Inmae (par Jeey)
           * oui il faut qu'ils soient là, à la prochaine réunion on définit les noms précis à utiliser.
           * TODO Angie ajouter les noms des champs FAIT
               * [https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/hosting.properties](https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/hosting.properties)


   * Ajout du champ organization.socialnetworks.pleroma
       * ajouté au fichier modèle organization.properties
       * TODO Cpm : implémenter StatoolInfos
       * pleroma = mastodon ? condisérer à part ? Ou introduire Fédivers ?
       * la demande initiale est de pouvoir valoriser les différentes communications des membres et donc avoir et le compte principale et les comptes secondaires, est pertinent


   * fiche service : champ service.website obligatoire
       * mais quand le service est la fourniture de comptes SSH (ou de DNS secondaires ou MixBackUp), il n'y a d'URL à donner et donc ça créé une erreur de remplissage
       * Solution : mettre l'URL d'une page web de présentation du service
       * TODO Cpm: ajouter un commentaire dans les modèles
       * FAIT angie (sur la fiche service.properties)  : 
           * *# Lien du site web du service (type URL, obligatoire - si pas possible, merci de créer une page de présentation du service).*


   * prévoir de communiquer sur l'usage possible des stats
       * Mettre à jour [https://wiki.chatons.org/doku.php/aider/gt\_stats.chatons.org](https://wiki.chatons.org/doku.php/aider/gt\_stats.chatons.org) pour la prochaine réunion mensuelle (mercredi 16/11)
       * Demander aux participant⋅es pourquoi iels ne remplissent pas leurs fiches


### Épilogue

Date prochaine réunion ?

   * 8 décembre
   * jeudi 11h15-13h00




## 70e réunion du groupe de travail

**Jeudi 08 décembre 2022 à 11h15**



Personnes présentes :

   * Cpm
   * Angie
   * Jeey
   * Immae


### Divers

   * RAS


### Revue de [https://stats.chatons.org/](https://stats.chatons.org/) 

   * page catégorie :
       * [https://stats.chatons.org/category-agregateurdefluxrss.xhtml](https://stats.chatons.org/category-agregateurdefluxrss.xhtml)
       * bug : n'affiche que 5 lignes au lieu de 7
       * bug aussi sur Agenda : 29 lignes au lieu de 31
       * TODO Cpm FAIT
   * **page générique d'un chaton** : 
       * penser à augmenter le code html avec les informations de properties pour faciliter le futur réagencement  UI/UX
           * TODO Cpm
   * **page « Statistiques » (fédération)** :
       * ajouter un donuts sur les services de paiement
           * TODO Cpm 
   * **un jour peut-être** :
       * pouvoir cliquer sur les graphiques pour voir la liste de résultats correspondant
           * par exemple pour les types d'inscription (à un service)
       * donuts sur les pays
           * pouvoir cliquer sur les résultats du camembert pour avoir une liste des chatons par pays
   * **pages Uptimes (Federation, Organization, Services)**
       * des améliorations à faire
           * TODO Cpm visibilité autres liens
           * TODO mrflos (pour l'été) : bidouiller la page statsuptime pour utiliser les filtres par état en js datatables
       * questions de statut manuel vs statut mesuré (page organization)
           * statut manuel seulement
           * statut mesuré seulement
           * les deux
           * un seul combiné des deux
           * discussion :
               * est-ce que la version manuelle est encore utile ? pertinence du mesuré
               * se poser la question de ce que cherche l'utilisateur
               * cas des statuts manuels « en travaux » ou  « fermé »
               * le statut manuel est plus important que le statut mesuré, respecté l'expression des admins
               * ne surtout pas afficher les deux
               * étudier la conjonction
               * garder l'un avec l'ajout d'un picto en plus si l'autres est KO
           * TODO Cpm voir pour une version « combinée » avec bulle informative
   * **Flo :**
       * peut être avoir dans résumé des moyennes sans graphes, genre du texte « sur 2020 XXX visiteurs uniques, YYY ips différentes »
           * Cpm : une notion de « tendance » ?
           * Cpm : donner exemple ?
           * TODO Flo, à réfléchir l'enrichissement de texte des graphes toujours en TODO  > GT le 20 juillet > décalé
           * TODO Flo, à réfléchir à des cadres de tendances dans « Résumé » toujours en  > GT le 20 juillet > décalé
       * changer les intitulés « Web » et « Spécifique » par « Graphes de visites web » ou plus court « Graphes Web » et « Statistiques propres aux services » ou plus court « Stats des services » ?
           * Cpm : préciser l'intention
           * Flo : expliquer les items du menu type > GT le 20 juillet (annulé cause covid...)  > décalé
           * TODO Cpm : ajouter des bulles
           * TODO Flo : tester le menu métriques auprès de personnes
               * en cours toujours en TODO
           * TODO réfléchir
       * TODO Cpm afficher les champs nom et description des métrics dans les diagrammes
   * **site statique vs site dynamique ?**
       * différence de besoin entre Libre-service.eu (métrique au jour) et le collectif CHATONS (mérique au mois)
       * supprimer les vues années, semaines et jours ?
       * supprimer les périodes 12 mois, 2020 et 2021 ?
       * site statique :
           * avantages : simplicité de déploiement (est-ce nécessaire ?)
           * inconvénients : prend de la place sur disque
       * pour site dynamique :
           * avantages : plus grande interactivité, plus de liberté fonctionnelles
           * inconvénients : nécessite l'installation d'un serveur d'application Java, du travail pour finaliser
       * avis :
           * Antoine : pas de problème au site dynamique
           * Flo : peut-être finir les fonctionnalités en cours avant de faire une version 2 dynamique avec des optimisations
   * **Test disponibilité en ipv6** :
       * [https://forum.chatons.org/t/suggestions-pour-la-version-0-6/3232](https://forum.chatons.org/t/suggestions-pour-la-version-0-6/3232)
       * est-ce une information déclarative ou à calculer ?
           * mieux si c'est calculé car plus à jour et un truc en moins à faire pour les admins
       * sujet non urgent, à approfondir
       * continuer à évaluer les besoins
       * page ipv6 pour avoir un aperçu du point de vue du collectif
   * **Vérifier la présence de trackers basic. **
       * [https://forum.chatons.org/t/suggestions-pour-la-version-0-6/3232](https://forum.chatons.org/t/suggestions-pour-la-version-0-6/3232)
       * oui, très pertinent
       * valorisation : à envisager page dédiée


### Revue générateur de fichiers métrics (statoolinfos)

   * ajout support Jitsi :
       * TODO Cpm en cours
   * ajout support Nextcloud
   * ajout support XMPP
   * metrics http
       * améliorer détection OS TODO Cpm
       * améliorer détection date TODO Cpm
   * pour info: mrflos travaille sur scrumblr et pourra faire la page stat
       * \o/
   * **App yunohost pour stats.chatons.org**
       * todo mrflos : contacter tobias a propos de l'appli yunohost chatons FAIT le 21 avril
       * ping (19/05/2022) FAIT 
       * réponse tobias
           * Salut,
           * J’ai complètement oublié de te répondre, j’ai presque rien fait, j’ai juste un début de python :
               * #! /usr/bin/python3
               * import sys
               * sys.path.insert(0, « /usr/lib/moulinette/ »)
               * import yunohost
               * yunohost.init\_logging()
               * yunohost.init\_i18n()
               * from yunohost.app import app\_info()from yunohost.app import app\_list()
               * apps = [ app[‹ id ›] for app in app\_list()[‹ apps ›] ]
           * Si vous organiser un repo, je voudrais bien le lien, je pourrais essayer de passer du temps dessus.
       * Créer un dépôt -> Angie le créé > FAIT (16/06)
           * [https://framagit.org/chatons/chatonsinfos-yunohost](https://framagit.org/chatons/chatonsinfos-yunohost)
           * trouver un animateur de ce dépôt (créer, surveiller, répondre etc..) -> voir avec Flo, Tobias ou Aleks
               * TODO CPM faire un appel sur le forum
           * TODO Angie : debrief  atelier camp
               * après relecture des CR d'ateliers, il s'avère que la question n'a pas été traitée lors du camp CHATONS. Relancer Flo, Tobias et Aleks ?
                   * OUI, il ne faut pas abandonner l'idée et cette liste de contact
                   * avec un « petit » cahier des charges, le futur contact sera plus efficace
                       * TODO CPM : rédiger un premier jet sur le pad proposé ci-dessous
           * idée de faire un cahier des charges (une simple liste de fonctionnalités)
               * pad à compléter : [https://mypads.framapad.org/p/cc-app-yunohost-stats-chatons-e4ikv7pj](https://mypads.framapad.org/p/cc-app-yunohost-stats-chatons-e4ikv7pj)
           * atelier pour faire un première échange oral
               * cpm, Aleks, Tobias 
               * Angie propose de caler un temps d'échange en début d'année 2023 pour discuter "faisabilité" 
                   * TODO Angie : envoyer un mp sur le forum FAIT


### Revue des catégories

   * références :
       * [https://stats.chatons.org/category-autres.xhtml](https://stats.chatons.org/category-autres.xhtml)
       * [https://framagit.org/chatons/chatonsinfos/-/blob/master/StatoolInfos/categories/categories.properties](https://framagit.org/chatons/chatonsinfos/-/blob/master/StatoolInfos/categories/categories.properties)
   * supprimer doublons :
       * signaturepdf, ApacheHttpd, File To Link, Diaspora*, Element-Web, jitsimeet, Open Street Map, rocket chat
       * TODO Cpm FAIT




### Revue des fichiers properties de membres

   * passer en revue :
       *  [https://stats.chatons.org/chatons-crawl.xhtml](https://stats.chatons.org/chatons-crawl.xhtml)
           * plus tard
       * [https://stats.chatons.org/chatons-propertyalerts.xhtml](https://stats.chatons.org/chatons-propertyalerts.xhtml)
           * rouges : 527 ~~562~~ ~~581~~ ~~586~~ ~~580~~ ~~495~~ ~~500~~ ~~498~~ ~~284~~ ~~289 ~~ ~~292~~  ~~316~~
           * jaunes :  1258 ~~1266~~ ~~1273~~ ~~1292~~ ~~1287~~ ~~1211~~ ~~1208~~ ~~1209~~ ~~1220~~  ~~1217~~  ~~1220~~  ~~1226~~ 
           * communiquer un jour prochain (au moment de la prochaine release - celle avec les metrics)
   * rapprochement avec fiches chatons.org (prospective) :
       * constat qu'il y a eu une convergence de la fiche service sur chatons.org et le fichier properties de service
       * comment pourrait s'envisager une étape supplémentaire de rapprochement ?
       * niveau organisation :
           * si on complète le formulaire de la fiche structure (?) sur chatons.org alors chatons.org serait à même de générer automatiquement le fichier properties de l'organisation
               * Si on automatise à la création d'une fiche, on va se retrouver avec les fiches des candidats chatons et autres huluberlus qui créent des fiches sans jamais candidater. Il faudrait donc que la génération n'intervienne que lorsqu'on modifie la valeur du champ `Etat de la candidature` en `chaton approuvé`    +1 +1
           * avantages :
               * un plus grand nombre d'organisations suivies dans stats.chatons.org
               * une gestion automatique
           * inconvénients :
               * besoin d'étudier des cas (suppression de fiche structure, sortie du collectif, etc.)
       *  niveau service :
           * si on complète le formulaire de la fiche service sur chatons.org alors chatons.org serait à même de générer automatiquement le fichier properties de service
               * comment la génération automatique de ces fiches service.properties viendront-elles alimenter la section Subs de fiches organization.properties ?
                   * très important oui
           * avantages :
               *  un plus grand nombre de services dans stats.chatons.org
               * une gestion automatique
           * besoin d'étudier des cas (suppression de fiche service, sortie du collectif, etc.)
           * Premier pas :
               * Rassembler les infos :
                   * Le nom de l'information et où se trouve-t-elle (si ça se trouve ? et où : fiche Chatons ? Properties) ?
       * TODO recensement comparatif + table de concordance, FAIT Angie \o/
           * [https://asso.framasoft.org/nextcloud/s/3cxYfqAms6Zg6pe](https://asso.framasoft.org/nextcloud/s/3cxYfqAms6Zg6pe)
       * TODO sur les prochaines réunions : définir où intégrer chaque champ de stats.chatons qui n'est pas encore présent sur chatons.org
   * cas stemy
       * [https://forum.chatons.org/t/stop-ou-encore/4196/10](https://forum.chatons.org/t/stop-ou-encore/4196/10)
       * [https://www.chatons.org/chatons/stemyme](https://www.chatons.org/chatons/stemyme)
       * invité à maj sa fiche CHATONS FAIT mrflos
       * TODO Angie modifier fichier proporties FAIT
   * cas automario
       * TODO Angie contact maj fichier properties  (organization.status)
       * en fait, semi-sommeil, il est moins dispo pour le collectif mais il s'occupe de faire tourner ses services, donc pas besoin de changer ses fiches
   * 3HG : fermeture, fichiers properties par défaut mis à jour par Angie


### Revue des tickets

   * [https://framagit.org/chatons/chatonsinfos/-/issues/1](https://framagit.org/chatons/chatonsinfos/-/issues/1)
       * Redesign des encarts au dessus des tableaux
       * prévu lorsqu'on aura toutes les informations affichées
       * statut : plus tard
   * [https://framagit.org/chatons/chatonsinfos/-/issues/2](https://framagit.org/chatons/chatonsinfos/-/issues/2)
       * Dans le tableau des services de la fiche organisation des chatons, supprimer la colonne "Organisation"
       * Cpm : ce tableau est une vue mutualisée entre plusieurs pages : organisation, services, catégorie,  logiciel ; l'information est effectivement redondante pour la page organisation, mais ça permet de conserver l'homogénéité de la vue.
       * Cpm : pour gagner de la place, possibilité de ne mettre que le logo de l'organisation et le nom en bulle
       * statut : réfléchir et sinon sera traité par la grande revue visuelle prévue un jour
   * [https://framagit.org/chatons/chatonsinfos/-/issues/4](https://framagit.org/chatons/chatonsinfos/-/issues/4)
       * Penser un nouveau fichier properties dédié aux offres non logicielles, celle d'hébergement
       * statut : priorité aux services utilisateurs donc pertinent mais plus tard


### Revue des merge requests

   * [https://framagit.org/chatons/chatonsinfos/-/merge\_requests](https://framagit.org/chatons/chatonsinfos/-/merge\_requests)
       * RAS


### Revue du forum

   * [https://forum.chatons.org/c/collectif/stats-chatons-org/83](https://forum.chatons.org/c/collectif/stats-chatons-org/83)
   * [https://forum.chatons.org/t/groupe-de-travail-metriques-mesures-de-co-2-pour-stats-chatons-org/4170](https://forum.chatons.org/t/groupe-de-travail-metriques-mesures-de-co-2-pour-stats-chatons-org/4170)
       * RAS
   * [https://forum.chatons.org/t/erreur-sur-laffichage-des-status-sur-la-page-structure/4347](https://forum.chatons.org/t/erreur-sur-laffichage-des-status-sur-la-page-structure/4347)
       * TODO répondre, FAIT mrflos
       * comment gérer les incohérences entre le statut déclaré et le statut technique ?
           * ajouter sur la page de chaque chaton un signe indiquant que le statut déclaré est contredit
           * TODO cpm




### Revue des disponibilités des services (uptimes)

   * [https://stats.chatons.org/chatons-uptimes.xhtml](https://stats.chatons.org/chatons-uptimes.xhtml)
   * Tchat > Colibris
       * connu, en cours de réinstallation
   * Visio > Colibris
       * Error code: SEC\_ERROR\_EXPIRED\_CERTIFICATE
       * TODO MrFlos : mettre à jour le certificat
   * Immae
       * les nouveaux services d'Immae qui n'ont pas la possibilité d'avoir des URL (comptes SSH et compte FTP)
       * Il est recommandé d'avoir du coup une page descriptive pour chacun des services
       * TODO Immae -> faire les pages de présentation de service
       * FAIT
   *   ExaCloud      Exarius
       * indisponibilité intermittente
       * à contacter la prochaine fois FAIT Angie 
       * juste 1 ou 2 alertes par jour, a priori pas bloquant
       * FAIT
   *   Tâches      Sans-nuage / ARN
       * depuis le 25/09, ~~indisponible~~ en fait très lent
       * [https://stats.chatons.org/sansnuagearn-taches-uptimes.xhtml](https://stats.chatons.org/sansnuagearn-taches-uptimes.xhtml)
       * TODO Mrflos contacter fait
       * fiche properties supprimée donc plus de problème
       * FAIT
   * VPN / VPS Sans-nuage /ARN
       * ce sont des offres de services avec URL bien mise mais en 404
       * TODO Angie contacter FAIT
   * Bastet
       * agenda, mobilizon, pad avancé, transfert d'image, contacts
       * rouges depuis le 31/10
       * TODO Angie contacter Dino -> contacté
       * réglé
   * DevlogPro
       * pad, peertube, video
       * rouges depuis plus de 2 semaines
       * TODO Immae contacter -> contacté
           * travaux en cours
           * PT toujours en rouge
           * TODO Jeey : savoir ce qu'il en est -> FAIT (08/12)
               * Réponse (08/12)
               * En effet, je l'ai éteinte, faute d'utilité :)
               * Je vais la supprimer de la conf CHATONS.
   *   Taskwarrior      ImmaeEu
       * rouge car 401  htaccess non encore géré par StatoolInfos
       * TODO Cpm FAIT
       * alerte disparu suite à l'amélioration du code StatoolInfos
       * réglé
   *   Peertube      UNDERWORLD
       * rouge depuis 3 jours, maj ?
       * TODO Jeey contacter -> FAIT (08/12)
           * Réponse (08/12):
           * Oui, c’est connu et maitrisé, et temporaire (disons encore 2/3 jours d’indispo avant le retour).




### Revue de l'ontologie

   * rappels :
       * l'ordre des questions à se poser : préfixe, sous-préfixe
       * on rajout .count si le mot signifiant est le préfixe
       * métriques HTTP :
           * contexte :
               * [https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/metrics.properties](https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/metrics.properties)
               * [http://www.webalizer.org/webalizer\_help.html](http://www.webalizer.org/webalizer\_help.html)
                   * [https://web.archive.org/web/20210330151216/http://www.webalizer.org/webalizer\_help.html](https://web.archive.org/web/20210330151216/http://www.webalizer.org/webalizer\_help.html)


   * mesure du CO2 :
       * en attente résultats du groupe de travail dédié
       * suite à la réunion du GT, l'élément le plus factuel serait la mesure de la consommation électrique (kWh) de chaque service. Sinon il n'y a pas de consensus sur les mesures.
       * cf un widget à la mode chez les lownumisateurices [https://www.websitecarbon.com/website/louisderrac-com-2021-11-03-eduquer-au-numerique-daccord-mais-pas-nimporte-lequel-et-pas-nimporte-comment/](https://www.websitecarbon.com/website/louisderrac-com-2021-11-03-eduquer-au-numerique-daccord-mais-pas-nimporte-lequel-et-pas-nimporte-comment/)
       * imaginer un système de grille pour agglomérer des indicateurs pour faire un indicateur
       * kwh et co2 ne sont pas suffisants, faut d'autres infos, âge machine, durée exploitation
       * certaines informations supplémentaires seraient donc nécessaires mais pas en tant que métrique mais fichier properties service


   * fiche properties hébergement
       * [https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/hosting.properties](https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/hosting.properties)
       * déjà 2 fichiers properties remplis !
       * projection d'intégration dans stats.chatons.org
           * besoin d'un bouton « Services dédiés » 
           * certainement renommer « Services » en « Services mutualisés » +1
           * page d'index : 
               * ajouter un nombre spécifique pour les services dédiés
               * ajouter un indicateur pour différencier
               * dans le tableau : un colonne pour les services dédiés / une pour les services mutualisés
           * page d'un membre :
               * ajouter un nombre spécifique pour les services dédiés
               * ajouter un indicateur pour différencier
           * nommage : service mutualisé vs service dédié
       * TODO intégrer dans le site stats.chatons.org
       * TODO communiquer auprès du collectif sur l'existence de cette nouvelle fiche


   * Ajout du champ organization.socialnetworks.pleroma
       * ajouté au fichier modèle organization.properties
       * TODO Cpm : implémenter StatoolInfos
       * pleroma = mastodon ? condisérer à part ? Ou introduire Fédivers ?
       * la demande initiale est de pouvoir valoriser les différentes communications des membres et donc avoir et le compte principale et les comptes secondaires, est pertinent


   * fiche service : champ service.website obligatoire
       * mais quand le service est la fourniture de comptes SSH (ou de DNS secondaires ou MixBackUp), il n'y a d'URL à donner et donc ça créé une erreur de remplissage
       * Solution : mettre l'URL d'une page web de présentation du service
       * FAIT angie (sur la fiche service.properties)  : 
           * *# Lien du site web du service (type URL, obligatoire - si pas possible, merci de créer une page de présentation du service).*
           * TODO Cpm propager aux autres modèles de service


   * prévoir de communiquer sur l'usage possible des stats
       * Mettre à jour [https://wiki.chatons.org/doku.php/aider/gt\_stats.chatons.org](https://wiki.chatons.org/doku.php/aider/gt\_stats.chatons.org) pour la prochaine réunion mensuelle (jeudi 15/12)
       * Demander aux participant⋅es pourquoi iels ne remplissent pas leurs fiches


### Épilogue

Date prochaine réunion ?

   * 12 janvier
   * jeudi 11h15-13h00


## 71e réunion du groupe de travail

**Jeudi 12 janvier 2023 à 11h15**



Personnes présentes :

   * Cpm
   * Angie
   * Jeey
   * Immae


### Divers

   * RAS


### Revue de [https://stats.chatons.org/](https://stats.chatons.org/) 

   * affichage des métriques : remplacement du bouton 2020 par 2022
   * **page générique d'un chaton** : 
       * penser à augmenter le code html avec les informations de properties pour faciliter le futur réagencement  UI/UX
           * TODO Cpm
   * **page « Statistiques » (fédération)** :
       * ajouter un donuts sur les services de paiement
           * TODO Cpm 
   * **un jour peut-être** :
       * pouvoir cliquer sur les graphiques pour voir la liste de résultats correspondant
           * par exemple pour les types d'inscription (à un service)
       * donuts sur les pays
           * pouvoir cliquer sur les résultats du camembert pour avoir une liste des chatons par pays
   * **pages Uptimes (Federation, Organization, Services)**
       * des améliorations à faire
           * TODO Cpm visibilité autres liens
           * TODO mrflos (pour l'été) : bidouiller la page statsuptime pour utiliser les filtres par état en js datatables
       * questions de statut manuel vs statut mesuré (page organization)
           * statut manuel seulement
           * statut mesuré seulement
           * les deux
           * un seul combiné des deux
           * discussion :
               * est-ce que la version manuelle est encore utile ? pertinence du mesuré
               * se poser la question de ce que cherche l'utilisateur
               * cas des statuts manuels « en travaux » ou  « fermé »
               * le statut manuel est plus important que le statut mesuré, respecté l'expression des admins
               * ne surtout pas afficher les deux
               * étudier la conjonction
               * garder l'un avec l'ajout d'un picto en plus si l'autres est KO
           * TODO Cpm voir pour une version « combinée » avec bulle informative
   * **Flo :**
       * peut être avoir dans résumé des moyennes sans graphes, genre du texte « sur 2020 XXX visiteurs uniques, YYY ips différentes »
           * Cpm : une notion de « tendance » ?
           * Cpm : donner exemple ?
           * TODO Flo, à réfléchir l'enrichissement de texte des graphes toujours en TODO  > GT le 20 juillet > décalé
           * TODO Flo, à réfléchir à des cadres de tendances dans « Résumé » toujours en  > GT le 20 juillet > décalé
       * changer les intitulés « Web » et « Spécifique » par « Graphes de visites web » ou plus court « Graphes Web » et « Statistiques propres aux services » ou plus court « Stats des services » ?
           * Cpm : préciser l'intention
           * Flo : expliquer les items du menu type > GT le 20 juillet (annulé cause covid...)  > décalé
           * TODO Cpm : ajouter des bulles
           * TODO Flo : tester le menu métriques auprès de personnes
               * en cours toujours en TODO
           * TODO réfléchir
       * TODO Cpm afficher les champs nom et description des métrics dans les diagrammes
   * **site statique vs site dynamique ?**
       * différence de besoin entre Libre-service.eu (métrique au jour) et le collectif CHATONS (mérique au mois)
       * supprimer les vues années, semaines et jours ?
       * supprimer les périodes 12 mois, 2020 et 2021 ?
       * site statique :
           * avantages : simplicité de déploiement (est-ce nécessaire ?)
           * inconvénients : prend de la place sur disque
       * pour site dynamique :
           * avantages : plus grande interactivité, plus de liberté fonctionnelles
           * inconvénients : nécessite l'installation d'un serveur d'application Java, du travail pour finaliser
       * avis :
           * Antoine : pas de problème au site dynamique
           * Flo : peut-être finir les fonctionnalités en cours avant de faire une version 2 dynamique avec des optimisations
   * **Test disponibilité en ipv6** :
       * [https://forum.chatons.org/t/suggestions-pour-la-version-0-6/3232](https://forum.chatons.org/t/suggestions-pour-la-version-0-6/3232)
       * est-ce une information déclarative ou à calculer ?
           * mieux si c'est calculé car plus à jour et un truc en moins à faire pour les admins
       * sujet non urgent, à approfondir
       * continuer à évaluer les besoins
       * page ipv6 pour avoir un aperçu du point de vue du collectif
   * **Vérifier la présence de trackers basic. **
       * [https://forum.chatons.org/t/suggestions-pour-la-version-0-6/3232](https://forum.chatons.org/t/suggestions-pour-la-version-0-6/3232)
       * oui, très pertinent
       * valorisation : à envisager page dédiée


### Revue générateur de fichiers métrics (statoolinfos)

   * ajout support Jitsi :
       * TODO Cpm en cours
   * ajout support Nextcloud
   * ajout support XMPP
   * metrics http
       * améliorer détection OS TODO Cpm
       * améliorer détection date TODO Cpm
   * pour info: mrflos travaille sur scrumblr et pourra faire la page stat
       * \o/
   * **App yunohost pour stats.chatons.org**
       * todo mrflos : contacter tobias a propos de l'appli yunohost chatons FAIT le 21 avril
       * ping (19/05/2022) FAIT 
       * réponse tobias
           * Salut,
           * J’ai complètement oublié de te répondre, j’ai presque rien fait, j’ai juste un début de python :
               * #! /usr/bin/python3
               * import sys
               * sys.path.insert(0, « /usr/lib/moulinette/ »)
               * import yunohost
               * yunohost.init\_logging()
               * yunohost.init\_i18n()
               * from yunohost.app import app\_info()from yunohost.app import app\_list()
               * apps = [ app[‹ id ›] for app in app\_list()[‹ apps ›] ]
           * Si vous organiser un repo, je voudrais bien le lien, je pourrais essayer de passer du temps dessus.
       * Créer un dépôt -> Angie le créé > FAIT (16/06)
           * [https://framagit.org/chatons/chatonsinfos-yunohost](https://framagit.org/chatons/chatonsinfos-yunohost)
           * trouver un animateur de ce dépôt (créer, surveiller, répondre etc..) -> voir avec Flo, Tobias ou Aleks
               * TODO CPM faire un appel sur le forum
           * TODO Angie : debrief  atelier camp
               * après relecture des CR d'ateliers, il s'avère que la question n'a pas été traitée lors du camp CHATONS. Relancer Flo, Tobias et Aleks ?
                   * OUI, il ne faut pas abandonner l'idée et cette liste de contact
                   * avec un « petit » cahier des charges, le futur contact sera plus efficace
                       * TODO CPM : rédiger un premier jet sur le pad proposé ci-dessous
           * idée de faire un cahier des charges (une simple liste de fonctionnalités)
               * pad à compléter : [https://mypads.framapad.org/p/cc-app-yunohost-stats-chatons-e4ikv7pj](https://mypads.framapad.org/p/cc-app-yunohost-stats-chatons-e4ikv7pj)
           * atelier pour faire un première échange oral
               * cpm, Aleks, Tobias 
               * Angie propose de caler un temps d'échange en début d'année 2023 pour discuter "faisabilité" 
                   * TODO Angie : envoyer un mp sur le forum FAIT
                   * pas encore de réponse concrête
                   * Angie en parle avec Tobias la semaine prochaine


### Revue des catégories

   * références :
       * [https://stats.chatons.org/category-autres.xhtml](https://stats.chatons.org/category-autres.xhtml)
       * [https://framagit.org/chatons/chatonsinfos/-/blob/master/StatoolInfos/categories/categories.properties](https://framagit.org/chatons/chatonsinfos/-/blob/master/StatoolInfos/categories/categories.properties)
   * autres :
       *  ntfy :
           * licence libre : OUI
           * Nouvelle catégorie à créer : Gestionnaire de notification
               * *##Gestionnaire de notifications*
               * categories.notifications.name=Gestionnaire de notifications
               * categories.notifications.description=
               * categories.notifications.logo=notifications.svg
               * categories.notifications.softwares=ntfy
               * TODO Angie FAIT
   * ajouter Paheco, le nouveau nom de Garradin
       * [https://garradin.eu/garradin-devient-paheko](https://garradin.eu/garradin-devient-paheko)
       * heu … y avait pas Garradin ?
       * TODO Angie FAIT
   * ajouter Forgejo : le fork de Gitea chez Codeberg
       * TODO Angie : FAIT


### Revue des fichiers properties de membres

   * passer en revue :
       *  [https://stats.chatons.org/chatons-crawl.xhtml](https://stats.chatons.org/chatons-crawl.xhtml)
           * plus tard
       * [https://stats.chatons.org/chatons-propertyalerts.xhtml](https://stats.chatons.org/chatons-propertyalerts.xhtml)
           * rouges : 536, ~~527~~ ~~562~~ ~~581~~ ~~586~~ ~~580~~ ~~495~~ ~~500~~ ~~498~~ ~~284~~ ~~289 ~~ ~~292~~  ~~316~~
           * jaunes :  1305, ~~1258~~ ~~1266~~ ~~1273~~ ~~1292~~ ~~1287~~ ~~1211~~ ~~1208~~ ~~1209~~ ~~1220~~  ~~1217~~  ~~1220~~  ~~1226~~ 
           * communiquer un jour prochain (au moment de la prochaine release - celle avec les metrics)
   * ajout stats 2023 dans le chatons.properties TODO Angie FAIT
   * nouveaux CHATONS portée n°15 :
       * création de fichier properties par défaut x6 TODO Angie FAIT
       * maj fichier properties CHATONS : FAIT (plus qu'à transférer sur le serveur)
   * membre en partance :
       * maj fichier properties
       * maj fichier properties CHATONS
       * les connus : stemy
       * les imprévus : amipo, outilsconviviaux
       * autres : anancus 
           * TODO Angie
           * + récupérer ses fichiers properties et les copier sur le serveur (+ modifier le subs de chatons.properties)
   * **rapprochement avec fiches chatons.org** (prospective) :
       * constat qu'il y a eu une convergence de la fiche service sur chatons.org et le fichier properties de service
       * comment pourrait s'envisager une étape supplémentaire de rapprochement ?
       * niveau organisation :
           * si on complète le formulaire de la fiche structure (?) sur chatons.org alors chatons.org serait à même de générer automatiquement le fichier properties de l'organisation
               * Si on automatise à la création d'une fiche, on va se retrouver avec les fiches des candidats chatons et autres huluberlus qui créent des fiches sans jamais candidater. Il faudrait donc que la génération n'intervienne que lorsqu'on modifie la valeur du champ `Etat de la candidature` en `chaton approuvé`    +1 +1
           * avantages :
               * un plus grand nombre d'organisations suivies dans stats.chatons.org
               * une gestion automatique
           * inconvénients :
               * besoin d'étudier des cas (suppression de fiche structure, sortie du collectif, etc.)
       *  niveau service :
           * si on complète le formulaire de la fiche service sur chatons.org alors chatons.org serait à même de générer automatiquement le fichier properties de service
               * comment la génération automatique de ces fiches service.properties viendront-elles alimenter la section Subs de fiches organization.properties ?
                   * très important oui
           * avantages :
               *  un plus grand nombre de services dans stats.chatons.org
               * une gestion automatique
           * besoin d'étudier des cas (suppression de fiche service, sortie du collectif, etc.)
           * Premier pas :
               * Rassembler les infos :
                   * Le nom de l'information et où se trouve-t-elle (si ça se trouve ? et où : fiche Chatons ? Properties) ?
       * TODO recensement comparatif + table de concordance, FAIT Angie \o/
           * [https://asso.framasoft.org/nextcloud/s/3cxYfqAms6Zg6pe](https://asso.framasoft.org/nextcloud/s/3cxYfqAms6Zg6pe)
       * TODO sur les prochaines réunions : définir où intégrer chaque champ de stats.chatons qui n'est pas encore présent sur chatons.org
           * Prochaine réunion : on créé les champs manquants côté chatons.org qui sont dans organisation.properties




### Revue des tickets

   * [https://framagit.org/chatons/chatonsinfos/-/issues/1](https://framagit.org/chatons/chatonsinfos/-/issues/1)
       * Redesign des encarts au dessus des tableaux
       * prévu lorsqu'on aura toutes les informations affichées
       * statut : plus tard
   * [https://framagit.org/chatons/chatonsinfos/-/issues/2](https://framagit.org/chatons/chatonsinfos/-/issues/2)
       * Dans le tableau des services de la fiche organisation des chatons, supprimer la colonne "Organisation"
       * Cpm : ce tableau est une vue mutualisée entre plusieurs pages : organisation, services, catégorie,  logiciel ; l'information est effectivement redondante pour la page organisation, mais ça permet de conserver l'homogénéité de la vue.
       * Cpm : pour gagner de la place, possibilité de ne mettre que le logo de l'organisation et le nom en bulle
       * statut : réfléchir et sinon sera traité par la grande revue visuelle prévue un jour
   * [https://framagit.org/chatons/chatonsinfos/-/issues/4](https://framagit.org/chatons/chatonsinfos/-/issues/4)
       * Penser un nouveau fichier properties dédié aux offres non logicielles, celle d'hébergement
       * statut : priorité aux services utilisateurs donc pertinent mais plus tard


### Revue des merge requests

   * [https://framagit.org/chatons/chatonsinfos/-/merge\_requests](https://framagit.org/chatons/chatonsinfos/-/merge\_requests)
       * RAS


### Revue du forum

   * [https://forum.chatons.org/c/collectif/stats-chatons-org/83](https://forum.chatons.org/c/collectif/stats-chatons-org/83)
   * [https://forum.chatons.org/t/erreur-sur-laffichage-des-status-sur-la-page-structure/4347](https://forum.chatons.org/t/erreur-sur-laffichage-des-status-sur-la-page-structure/4347)
       * TODO répondre, FAIT mrflos
       * comment gérer les incohérences entre le statut déclaré et le statut technique ?
           * ajouter sur la page de chaque chaton un signe indiquant que le statut déclaré est contredit
           * TODO cpm




### Revue des disponibilités des services (uptimes)

   * [https://stats.chatons.org/chatons-uptimes.xhtml](https://stats.chatons.org/chatons-uptimes.xhtml)
   * Tchat > Colibris
       * connu, en cours de réinstallation
       * toujours en cours de réflexion de réinstallation...
   * Visio > Colibris
       * Error code: SEC\_ERROR\_EXPIRED\_CERTIFICATE
       * TODO MrFlos : mettre à jour le certificat FAIT
   * VPN / VPS Sans-nuage /ARN
       * ce sont des offres de services avec URL bien mise mais en 404
       * TODO Angie contacter FAIT
       * toujours rouge  pour VPS donc Angie refait un MP - FAIT
   * DevlogPro
       * pad, peertube, video
       * rouges depuis plus de 2 semaines
       * TODO Immae contacter -> contacté
           * travaux en cours
           * PT toujours en rouge
           * TODO Jeey : savoir ce qu'il en est -> FAIT (08/12)
               * Réponse (08/12)
               * En effet, je l'ai éteinte, faute d'utilité :)
               * Je vais la supprimer de la conf CHATONS.
           * pad et video OK
           * PeerTube tout rouge les 3 dernières semaines
           * TODO Immae relance 
   *   Peertube      UNDERWORLD
       * rouge depuis 3 jours, maj ?
       * TODO Jeey contacter -> FAIT (08/12)
           * Réponse (08/12):
           * Oui, c’est connu et maitrisé, et temporaire (disons encore 2/3 jours d’indispo avant le retour).
           * FAIT
   * Wallabag de Kaihuri qui clignote
       * TODO Angie encourager à créer fiche CHATONS
       * 7 fiches services sur [https://stats.chatons.org/kaihuri.xhtml](https://stats.chatons.org/kaihuri.xhtml) à mettre sur chatons.org
       * recommandation de rediriger zoocoop.com vers kaihuri.org
       * FAIT le 12/01


### Revue de l'ontologie

   * rappels :
       * l'ordre des questions à se poser : préfixe, sous-préfixe
       * on rajout .count si le mot signifiant est le préfixe
       * métriques HTTP :
           * contexte :
               * [https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/metrics.properties](https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/metrics.properties)
               * [http://www.webalizer.org/webalizer\_help.html](http://www.webalizer.org/webalizer\_help.html)
                   * [https://web.archive.org/web/20210330151216/http://www.webalizer.org/webalizer\_help.html](https://web.archive.org/web/20210330151216/http://www.webalizer.org/webalizer\_help.html)


   * mesure du CO2 :
       * en attente résultats du groupe de travail dédié
       * suite à la réunion du GT, l'élément le plus factuel serait la mesure de la consommation électrique (kWh) de chaque service. Sinon il n'y a pas de consensus sur les mesures.
       * cf un widget à la mode chez les lownumisateurices [https://www.websitecarbon.com/website/louisderrac-com-2021-11-03-eduquer-au-numerique-daccord-mais-pas-nimporte-lequel-et-pas-nimporte-comment/](https://www.websitecarbon.com/website/louisderrac-com-2021-11-03-eduquer-au-numerique-daccord-mais-pas-nimporte-lequel-et-pas-nimporte-comment/)
       * imaginer un système de grille pour agglomérer des indicateurs pour faire un indicateur
       * kwh et co2 ne sont pas suffisants, faut d'autres infos, âge machine, durée exploitation
       * certaines informations supplémentaires seraient donc nécessaires mais pas en tant que métrique mais fichier properties service


   * fiche properties hébergement
       * [https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/hosting.properties](https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/hosting.properties)
       * déjà 2 fichiers properties remplis !
       * projection d'intégration dans stats.chatons.org
           * besoin d'un bouton « Services dédiés » 
           * certainement renommer « Services » en « Services mutualisés » +1
           * page d'index : 
               * ajouter un nombre spécifique pour les services dédiés
               * ajouter un indicateur pour différencier
               * dans le tableau : un colonne pour les services dédiés / une pour les services mutualisés
           * page d'un membre :
               * ajouter un nombre spécifique pour les services dédiés
               * ajouter un indicateur pour différencier
           * nommage : service mutualisé vs service dédié
       * TODO intégrer dans le site stats.chatons.org
       * TODO communiquer auprès du collectif sur l'existence de cette nouvelle fiche


   * Ajout du champ organization.socialnetworks.pleroma
       * ajouté au fichier modèle organization.properties
       * TODO Cpm : implémenter StatoolInfos
       * pleroma = mastodon ? condisérer à part ? Ou introduire Fédivers ?
       * la demande initiale est de pouvoir valoriser les différentes communications des membres et donc avoir et le compte principale et les comptes secondaires, est pertinent


   * fiche service : champ service.website obligatoire
       * mais quand le service est la fourniture de comptes SSH (ou de DNS secondaires ou MixBackUp), il n'y a d'URL à donner et donc ça créé une erreur de remplissage
       * Solution : mettre l'URL d'une page web de présentation du service
       * FAIT angie (sur la fiche service.properties)  : 
           * *# Lien du site web du service (type URL, obligatoire - si pas possible, merci de créer une page de présentation du service).*
           * TODO Cpm propager aux autres modèles de service


   * prévoir de communiquer sur l'usage possible des stats
       * Mettre à jour [https://wiki.chatons.org/doku.php/aider/gt\_stats.chatons.org](https://wiki.chatons.org/doku.php/aider/gt\_stats.chatons.org) pour la prochaine réunion mensuelle (jeudi 15/12)
       * Demander aux participant⋅es pourquoi iels ne remplissent pas leurs fiches


### Épilogue

Date prochaine réunion ?

   * jeudi 9 février
   * jeudi 11h15-13h00




## 72e réunion du groupe de travail

**Jeudi 9 février 2023 à 11h15**



Personnes présentes :

   * Angie
   * Cpm
   * Immae
   * 



### Divers

   * gestion du pad :
       * lenteurs du pad car nombre de lignes important -> TODO Angie
       * hypothèses d'utiliser l'historique dynamique ? Marquer des étoiles :o)
   * accompagnements au déploiement de StatoolInfos (Cpm) :
       * Linux07 et Ti-nuage
   * Angie a modifié le fichier automario.properties et chatons.properties pour qu'il ne soit plus comptabilisé




## **Rapprochement avec fiches chatons.org**



   * Recensement comparatif + table de concordance : 
       * [https://asso.framasoft.org/nextcloud/s/3cxYfqAms6Zg6pe](https://asso.framasoft.org/nextcloud/s/3cxYfqAms6Zg6pe)


   * définir où intégrer chaque champ de stats.chatons qui n'est pas encore présent sur chatons.org
       * créer les champs manquants côté chatons.org qui sont dans organisation.properties


**champs de stats.chatons.org absents de chatons.org**

   * organization.owner.name
       * exemple : Chapril est le chaton de l'April
       * champ non obligatoire
       * dans la fiche structure
       * ~~"structure dont le chaton fait partie"~~
       * ~~"structure / organisation / entité parente"~~
       * ~~titre : "nom de la structure parente" (commentaire)~~
       * titre : nom de l'organisation fondatrice du chaton
   * organization.owner.website
       * titre : site web de l'organisation fondatrice du chaton
   * organization.owner.logo
       * titre : logo de l'organisation fondatrice du chaton
       * champs pour téléverser une image (et pas demande d'une URL)
       * ⚠️ ne pas afficher ce champ dans la vue


   * organization.contact.url
       * titre : page de contact
       * non obligatoire
       * fiche structure


   * organization.legal.url
       * titre : mentions légales
       * fiche fonctionnement
       * obligatoire


   * organization.guide.user
       * titre : lien vers la documentation utilisateur
       * fiche infrastructure
       * non obligatoire


   * organization.status.level
       * champ qui se complète automatiquement sur stats.chatons. org en précisant systématiquement ACTIVE


   * organization.status.description
       * toujours indiquer : chaton en activité


   * organization.enddate
       * la vraie date n'est pas nécessairement déduisible du changement en base du champ STATUS => pertinence de rajouter le champ
       * titre : date de fermeture du chaton
       * fiche fonctionnement
       * non obligatoire


   * organization.memberof.chatons.enddate
       * titre : date de sortie du collectif CHATONS
       * fiche fonctionnement
       * non obligatoire


   * organization.memberof.chatons.status.level
       * valeur de stats :
           * ACTIVE, IDLE, AWAY
       * valeurs de fiche :
           * en activité : ACTIVE
           * en création : non exprimé en fichier properties
           * en sommeil : IDLE
           * fermé : AWAY
           * radié : AWAY
       * à générer automatiquement à partir du champ "Statut de la structure (réservé à l'administration)"
           * si valeurs "radié" ou "parti" -> organization.memberof.chatons.status.level= AWAY
           * si valeur = "en activité" -> organization.memberof.chatons.status.level= ACTIVE
           * si valeur = "en sommeil" -> organization.memberof.chatons.status.level= IDLE


   * organization.memberof.chatons.status.description
       * en activité
       * en sommeil
       * a quitté le collectif


   * organization.country.code
       * déduit du champ "Pays" sur stats.chatons.org


   * organization.geolocation.latitude et organization.geolocation.longitude
       * déduction automatique des coordonnées geo à partir des champs "ville" et "code postal" de chatons.org


   * organization.geolocation.address
       * Adresse postale : champ libre
       * non obligatoire
       * fiche localisation


   * organization.funding.* (services de paiement permettant de récolter des donations)
       * à traiter comme les 2 champs (url + nom du media social) des médias sociaux
       * voir si possible d'avoir une liste fermée pour le nom


TODO Angie : créer les champs dans la structure de fiches

Les compléter pour 3 chatons en test (Framasoft / Immae / Libre-service.eu)







### Revue de [https://stats.chatons.org/](https://stats.chatons.org/) 

   * **page générique d'un chaton** : 
       * penser à augmenter le code html avec les informations de properties pour faciliter le futur réagencement  UI/UX
           * TODO Cpm
   * **page « Statistiques » (fédération)** :
       * ajouter un donuts sur les services de paiement
           * TODO Cpm 
   * **un jour peut-être** :
       * pouvoir cliquer sur les graphiques pour voir la liste de résultats correspondant
           * par exemple pour les types d'inscription (à un service)
       * donuts sur les pays
           * pouvoir cliquer sur les résultats du camembert pour avoir une liste des chatons par pays
   * **pages Uptimes (Federation, Organization, Services)**
       * des améliorations à faire
           * TODO Cpm visibilité autres liens
           * TODO mrflos (pour l'été) : bidouiller la page statsuptime pour utiliser les filtres par état en js datatables
       * questions de statut manuel vs statut mesuré (page organization)
           * statut manuel seulement
           * statut mesuré seulement
           * les deux
           * un seul combiné des deux
           * discussion :
               * est-ce que la version manuelle est encore utile ? pertinence du mesuré
               * se poser la question de ce que cherche l'utilisateur
               * cas des statuts manuels « en travaux » ou  « fermé »
               * le statut manuel est plus important que le statut mesuré, respecté l'expression des admins
               * ne surtout pas afficher les deux
               * étudier la conjonction
               * garder l'un avec l'ajout d'un picto en plus si l'autres est KO
           * TODO Cpm voir pour une version « combinée » avec bulle informative
   * **Flo :**
       * peut être avoir dans résumé des moyennes sans graphes, genre du texte « sur 2020 XXX visiteurs uniques, YYY ips différentes »
           * Cpm : une notion de « tendance » ?
           * Cpm : donner exemple ?
           * TODO Flo, à réfléchir l'enrichissement de texte des graphes toujours en TODO  > GT le 20 juillet > décalé
           * TODO Flo, à réfléchir à des cadres de tendances dans « Résumé » toujours en  > GT le 20 juillet > décalé
       * changer les intitulés « Web » et « Spécifique » par « Graphes de visites web » ou plus court « Graphes Web » et « Statistiques propres aux services » ou plus court « Stats des services » ?
           * Cpm : préciser l'intention
           * Flo : expliquer les items du menu type > GT le 20 juillet (annulé cause covid...)  > décalé
           * TODO Cpm : ajouter des bulles
           * TODO Flo : tester le menu métriques auprès de personnes
               * en cours toujours en TODO
           * TODO réfléchir
       * TODO Cpm afficher les champs nom et description des métrics dans les diagrammes
   * **site statique vs site dynamique ?**
       * différence de besoin entre Libre-service.eu (métrique au jour) et le collectif CHATONS (mérique au mois)
       * supprimer les vues années, semaines et jours ?
       * supprimer les périodes 12 mois, 2020 et 2021 ?
       * site statique :
           * avantages : simplicité de déploiement (est-ce nécessaire ?)
           * inconvénients : prend de la place sur disque
       * pour site dynamique :
           * avantages : plus grande interactivité, plus de liberté fonctionnelles
           * inconvénients : nécessite l'installation d'un serveur d'application Java, du travail pour finaliser
       * avis :
           * Antoine : pas de problème au site dynamique
           * Flo : peut-être finir les fonctionnalités en cours avant de faire une version 2 dynamique avec des optimisations
   * **Test disponibilité en ipv6** :
       * [https://forum.chatons.org/t/suggestions-pour-la-version-0-6/3232](https://forum.chatons.org/t/suggestions-pour-la-version-0-6/3232)
       * est-ce une information déclarative ou à calculer ?
           * mieux si c'est calculé car plus à jour et un truc en moins à faire pour les admins
       * sujet non urgent, à approfondir
       * continuer à évaluer les besoins
       * page ipv6 pour avoir un aperçu du point de vue du collectif
   * **Vérifier la présence de trackers basic. **
       * [https://forum.chatons.org/t/suggestions-pour-la-version-0-6/3232](https://forum.chatons.org/t/suggestions-pour-la-version-0-6/3232)
       * oui, très pertinent
       * valorisation : à envisager page dédiée


### Revue générateur de fichiers métrics (statoolinfos)

   * ajout support Jitsi :
       * TODO Cpm en cours
   * ajout support Nextcloud
   * ajout support XMPP
   * metrics http
       * améliorer détection OS TODO Cpm
       * améliorer détection date TODO Cpm
   * pour info: mrflos travaille sur scrumblr et pourra faire la page stat
       * \o/
   * **App yunohost pour stats.chatons.org**
       * todo mrflos : contacter tobias a propos de l'appli yunohost chatons FAIT le 21 avril
       * ping (19/05/2022) FAIT 
       * réponse tobias
           * Salut,
           * J’ai complètement oublié de te répondre, j’ai presque rien fait, j’ai juste un début de python :
               * #! /usr/bin/python3
               * import sys
               * sys.path.insert(0, « /usr/lib/moulinette/ »)
               * import yunohost
               * yunohost.init\_logging()
               * yunohost.init\_i18n()
               * from yunohost.app import app\_info()from yunohost.app import app\_list()
               * apps = [ app[‹ id ›] for app in app\_list()[‹ apps ›] ]
           * Si vous organiser un repo, je voudrais bien le lien, je pourrais essayer de passer du temps dessus.
       * Créer un dépôt -> Angie le créé > FAIT (16/06)
           * [https://framagit.org/chatons/chatonsinfos-yunohost](https://framagit.org/chatons/chatonsinfos-yunohost)
           * trouver un animateur de ce dépôt (créer, surveiller, répondre etc..) -> voir avec Flo, Tobias ou Aleks
               * TODO CPM faire un appel sur le forum
           * TODO Angie : debrief  atelier camp
               * après relecture des CR d'ateliers, il s'avère que la question n'a pas été traitée lors du camp CHATONS. Relancer Flo, Tobias et Aleks ?
                   * OUI, il ne faut pas abandonner l'idée et cette liste de contact
                   * avec un « petit » cahier des charges, le futur contact sera plus efficace
                       * TODO CPM : rédiger un premier jet sur le pad proposé ci-dessous
           * idée de faire un cahier des charges (une simple liste de fonctionnalités)
               * pad à compléter : [https://mypads.framapad.org/p/cc-app-yunohost-stats-chatons-e4ikv7pj](https://mypads.framapad.org/p/cc-app-yunohost-stats-chatons-e4ikv7pj)
           * atelier pour faire un première échange oral
               * cpm, Aleks, Tobias 
               * Angie propose de caler un temps d'échange en début d'année 2023 pour discuter "faisabilité" 
                   * TODO Angie : envoyer un mp sur le forum FAIT
                   * pas encore de réponse concrête
                   * Angie en parle avec Tobias la semaine prochaine


### Revue des catégories

   * références :
       * [https://stats.chatons.org/category-autres.xhtml](https://stats.chatons.org/category-autres.xhtml)
       * [https://framagit.org/chatons/chatonsinfos/-/blob/master/StatoolInfos/categories/categories.properties](https://framagit.org/chatons/chatonsinfos/-/blob/master/StatoolInfos/categories/categories.properties)
   * autres : RAS


### Revue des fichiers properties de membres

   * passer en revue :
       *  [https://stats.chatons.org/chatons-crawl.xhtml](https://stats.chatons.org/chatons-crawl.xhtml)
           * plus tard
       * [https://stats.chatons.org/chatons-propertyalerts.xhtml](https://stats.chatons.org/chatons-propertyalerts.xhtml)
           * rouges : 536, ~~527~~ ~~562~~ ~~581~~ ~~586~~ ~~580~~ ~~495~~ ~~500~~ ~~498~~ ~~284~~ ~~289 ~~ ~~292~~  ~~316~~
           * jaunes :  1305, ~~1258~~ ~~1266~~ ~~1273~~ ~~1292~~ ~~1287~~ ~~1211~~ ~~1208~~ ~~1209~~ ~~1220~~  ~~1217~~  ~~1220~~  ~~1226~~ 
           * communiquer un jour prochain (au moment de la prochaine release - celle avec les metrics)
   * membre en partance :
       * autres : anancus 
           * TODO Angie
           * + récupérer ses fichiers properties et les copier sur le serveur (+ modifier le subs de chatons.properties)






### Revue des tickets

   * [https://framagit.org/chatons/chatonsinfos/-/issues/1](https://framagit.org/chatons/chatonsinfos/-/issues/1)
       * Redesign des encarts au dessus des tableaux
       * prévu lorsqu'on aura toutes les informations affichées
       * statut : plus tard
   * [https://framagit.org/chatons/chatonsinfos/-/issues/2](https://framagit.org/chatons/chatonsinfos/-/issues/2)
       * Dans le tableau des services de la fiche organisation des chatons, supprimer la colonne "Organisation"
       * Cpm : ce tableau est une vue mutualisée entre plusieurs pages : organisation, services, catégorie,  logiciel ; l'information est effectivement redondante pour la page organisation, mais ça permet de conserver l'homogénéité de la vue.
       * Cpm : pour gagner de la place, possibilité de ne mettre que le logo de l'organisation et le nom en bulle
       * statut : réfléchir et sinon sera traité par la grande revue visuelle prévue un jour
   * [https://framagit.org/chatons/chatonsinfos/-/issues/4](https://framagit.org/chatons/chatonsinfos/-/issues/4)
       * Penser un nouveau fichier properties dédié aux offres non logicielles, celle d'hébergement
       * statut : priorité aux services utilisateurs donc pertinent mais plus tard


### Revue des merge requests

   * [https://framagit.org/chatons/chatonsinfos/-/merge\_requests](https://framagit.org/chatons/chatonsinfos/-/merge\_requests)
       * Linux07 :
           * [https://framagit.org/chatons/chatonsinfos/-/merge\_requests/47](https://framagit.org/chatons/chatonsinfos/-/merge\_requests/47)
           * Cpm : FAIT
       * Bechamail :
           * [https://framagit.org/chatons/chatonsinfos/-/merge\_requests/12#note\_1926726](https://framagit.org/chatons/chatonsinfos/-/merge\_requests/12#note\_1926726)
           * demande de mise à jour
           * Cpm : FAIT


### Revue du forum

   * [https://forum.chatons.org/c/collectif/stats-chatons-org/83](https://forum.chatons.org/c/collectif/stats-chatons-org/83)
   * [https://forum.chatons.org/t/erreur-sur-laffichage-des-status-sur-la-page-structure/4347](https://forum.chatons.org/t/erreur-sur-laffichage-des-status-sur-la-page-structure/4347)
       * TODO répondre, FAIT mrflos
       * comment gérer les incohérences entre le statut déclaré et le statut technique ?
           * ajouter sur la page de chaque chaton un signe indiquant que le statut déclaré est contredit
           * TODO cpm




### Revue des disponibilités des services (uptimes)

   * [https://stats.chatons.org/chatons-uptimes.xhtml](https://stats.chatons.org/chatons-uptimes.xhtml)
   * Tchat > Colibris
       * connu, en cours de réinstallation
       * toujours en cours de réflexion de réinstallation...
   * VPN / VPS Sans-nuage /ARN
       * ce sont des offres de services avec URL bien mise mais en 404
       * TODO Angie contacter FAIT
       * toujours rouge  pour VPS donc Angie refait un MP
   * DevlogPro
       * pad, peertube, video
       * rouges depuis plus de 2 semaines
       * TODO Immae contacter -> contacté
           * travaux en cours
           * PT toujours en rouge
           * TODO Jeey : savoir ce qu'il en est -> FAIT (08/12)
               * Réponse (08/12)
               * En effet, je l'ai éteinte, faute d'utilité :)
               * Je vais la supprimer de la conf CHATONS.
           * pad et video OK
           * PeerTube tout rouge les 3 dernières semaines
           * TODO Immae relance 
   * Wallabag de Kaihuri qui clignote
       * TODO Angie encourager à créer fiche CHATONS
       * 7 fiches services sur [https://stats.chatons.org/kaihuri.xhtml](https://stats.chatons.org/kaihuri.xhtml) à mettre sur chatons.org
       * recommandation de rediriger zoocoop.com vers kaihuri.org


### Revue de l'ontologie

   * rappels :
       * l'ordre des questions à se poser : préfixe, sous-préfixe
       * on rajout .count si le mot signifiant est le préfixe
       * métriques HTTP :
           * contexte :
               * [https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/metrics.properties](https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/metrics.properties)
               * [http://www.webalizer.org/webalizer\_help.html](http://www.webalizer.org/webalizer\_help.html)
                   * [https://web.archive.org/web/20210330151216/http://www.webalizer.org/webalizer\_help.html](https://web.archive.org/web/20210330151216/http://www.webalizer.org/webalizer\_help.html)


   * mesure du CO2 :
       * en attente résultats du groupe de travail dédié
       * suite à la réunion du GT, l'élément le plus factuel serait la mesure de la consommation électrique (kWh) de chaque service. Sinon il n'y a pas de consensus sur les mesures.
       * cf un widget à la mode chez les lownumisateurices [https://www.websitecarbon.com/website/louisderrac-com-2021-11-03-eduquer-au-numerique-daccord-mais-pas-nimporte-lequel-et-pas-nimporte-comment/](https://www.websitecarbon.com/website/louisderrac-com-2021-11-03-eduquer-au-numerique-daccord-mais-pas-nimporte-lequel-et-pas-nimporte-comment/)
       * imaginer un système de grille pour agglomérer des indicateurs pour faire un indicateur
       * kwh et co2 ne sont pas suffisants, faut d'autres infos, âge machine, durée exploitation
       * certaines informations supplémentaires seraient donc nécessaires mais pas en tant que métrique mais fichier properties service


   * fiche properties hébergement
       * [https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/hosting.properties](https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/hosting.properties)
       * déjà 2 fichiers properties remplis !
       * projection d'intégration dans stats.chatons.org
           * besoin d'un bouton « Services dédiés » 
           * certainement renommer « Services » en « Services mutualisés » +1
           * page d'index : 
               * ajouter un nombre spécifique pour les services dédiés
               * ajouter un indicateur pour différencier
               * dans le tableau : un colonne pour les services dédiés / une pour les services mutualisés
           * page d'un membre :
               * ajouter un nombre spécifique pour les services dédiés
               * ajouter un indicateur pour différencier
           * nommage : service mutualisé vs service dédié
       * TODO intégrer dans le site stats.chatons.org
       * TODO communiquer auprès du collectif sur l'existence de cette nouvelle fiche


   * Ajout du champ organization.socialnetworks.pleroma
       * ajouté au fichier modèle organization.properties
       * TODO Cpm : implémenter StatoolInfos
       * pleroma = mastodon ? condisérer à part ? Ou introduire Fédivers ?
       * la demande initiale est de pouvoir valoriser les différentes communications des membres et donc avoir et le compte principale et les comptes secondaires, est pertinent


   * fiche service : champ service.website obligatoire
       * mais quand le service est la fourniture de comptes SSH (ou de DNS secondaires ou MixBackUp), il n'y a d'URL à donner et donc ça créé une erreur de remplissage
       * Solution : mettre l'URL d'une page web de présentation du service
       * FAIT angie (sur la fiche service.properties)  : 
           * *# Lien du site web du service (type URL, obligatoire - si pas possible, merci de créer une page de présentation du service).*
           * TODO Cpm propager aux autres modèles de service


   * prévoir de communiquer sur l'usage possible des stats
       * Mettre à jour [https://wiki.chatons.org/doku.php/aider/gt\_stats.chatons.org](https://wiki.chatons.org/doku.php/aider/gt\_stats.chatons.org) pour la prochaine réunion mensuelle (jeudi 15/12)
       * Demander aux participant⋅es pourquoi iels ne remplissent pas leurs fiches


### Épilogue

Date prochaine réunion ?

   * jeudi 13 avril
   * 11h15-13h00
   * sur [https://bbb.paquerette.eu/b/dom-67q-irl-nfc](https://bbb.paquerette.eu/b/dom-67q-irl-nfc)






## 73e réunion du groupe de travail

**Jeudi 13 avril 2023 à 11h15**



Personnes présentes :

   * Angie
   * Cpm
   * Immae


### Divers

   * gestion du pad :
       * lenteurs du pad car nombre de lignes important -> TODO Angie
       * hypothèses d'utiliser l'historique dynamique ? Marquer des étoiles :o)
   * accompagnements au déploiement de StatoolInfos (Cpm) :
       * Linux07 et Ti-nuage
   * Angie a modifié le fichier chatons.properties pour qu'il soit à jour (retrait des 5 chatons radiés)




## **Rapprochement avec fiches chatons.org**



   * Recensement comparatif + table de concordance : 
       * MAJ
       * [https://asso.framasoft.org/nextcloud/s/b6GkkXW5Zb8eijT](https://asso.framasoft.org/nextcloud/s/b6GkkXW5Zb8eijT)


   * définir où intégrer chaque champ de stats.chatons qui n'est pas encore présent sur chatons.org
       * créer les champs manquants côté chatons.org qui sont dans organisation.properties


**champs de stats.chatons.org absents de chatons.org**

   * organization.owner.name
       * exemple : Chapril est le chaton de l'April
       * champ non obligatoire
       * dans la fiche structure
       * titre : nom de l'organisation fondatrice du chaton
       * FAIT


   * organization.owner.website
       * titre : site web de l'organisation fondatrice du chaton
       * FAIT


   * organization.owner.logo
       * titre : logo de l'organisation fondatrice du chaton
       * champs pour téléverser une image (et pas demande d'une URL)
       * ⚠️ ne pas afficher ce champ dans la vue
       * FAIT


   * organization.contact.url
       * titre : page de contact
       * non obligatoire
       * fiche structure
       * FAIT


   * organization.legal.url
       * titre : mentions légales
       * fiche fonctionnement
       * obligatoire
       * FAIT


   * organization.guide.user
       * titre : lien vers la documentation utilisateur
       * fiche infrastructure
       * non obligatoire
       * FAIT


   * organization.status.level
       * champ qui se complète automatiquement sur stats.chatons. org en précisant systématiquement ACTIVE (uniquement sur les fiches publiées)


   * organization.status.description
       * toujours indiquer : chaton en activité


   * organization.enddate
       * la vraie date n'est pas nécessairement déduisible du changement en base du champ STATUS => pertinence de rajouter le champ
       * titre : date de fermeture du chaton
       * fiche fonctionnement
       * non obligatoire
       * FAIT


   * organization.memberof.chatons.enddate
       * titre : date de sortie du collectif CHATONS
       * fiche fonctionnement
       * non obligatoire
       * FAIT


   * organization.memberof.chatons.status.level
       * valeur de stats :
           * ACTIVE, IDLE, AWAY
       * valeurs de fiche :
           * en activité : ACTIVE
           * en création : non exprimé en fichier properties
           * en sommeil : IDLE
           * fermé : AWAY
           * radié : AWAY
       * à générer automatiquement à partir du champ "Statut de la structure (réservé à l'administration)"
           * si valeurs "radié" ou "parti" -> organization.memberof.chatons.status.level= AWAY
           * si valeur = "en activité" -> organization.memberof.chatons.status.level= ACTIVE
           * si valeur = "en sommeil" -> organization.memberof.chatons.status.level= IDLE


   * organization.memberof.chatons.status.description
       * valeur déduite de organization.memberof.chatons.status.level
       * en activité
       * en sommeil
       * a quitté le collectif


   * organization.country.code
       * déduit du champ "Pays" sur stats.chatons.org


   * organization.geolocation.latitude et organization.geolocation.longitude
       * déduction automatique des coordonnées geo à partir des champs "ville" et "code postal" de chatons.org


   * organization.geolocation.address
       * Adresse postale : champ libre
       * non obligatoire
       * fiche localisation
       * FAIT


   * organization.funding.* (services de paiement permettant de récolter des donations)
       * à traiter comme les 2 champs (url + nom du media social) des médias sociaux
       * voir si possible d'avoir une liste fermée pour le nom
       * FAIT (pas de liste fermée pour le moment)


TODO Angie : créer les champs dans la structure de fiches FAIT

Les compléter pour 3 chatons en test (Framasoft / Immae / Libre-service.eu) complété pour Framasoft



**Champs de service.properties à intégrer dans chatons.org**

à priori tout va aller dans la fiche "logiciel" sur chatons.org



   * service.name -> nom du service (obligatoire)
   * service.description -> description du service
   * service.logo -> logo du service (à ne pas afficher dans la vue)
   * service.legal.url -> mentions légales du service (lien vers)
   * service.guide.technical -> documentation technique du service (lien vers)
   * service.guide.user -> documentation utilisateur⋅ice du service (lien vers)
   * service.contact.url -> page web du support du service (lien vers)
   * service.contact.email -> courriel du support du service
   * service.startdate -> date d'ouverture du service
   * service.enddate -> date de fermeture du service
   * service.status.level -> statut du service (obligatoire)
       * créer une liste déroulante avec les choix suivants : 
           * OK : tout va bien (service en fonctionnement nominal).
           * WARNING : attention (service potentiellement incomplet, maintenance prévue, etc.).
           * ALERT : alerte (le service connaît des dysfonctionnements, le service va bientôt fermer, etc.).
           * ERROR : problème majeur (service en panne).
           * OVER : terminé (le service n'existe plus).
           * VOID : indéterminé (service non ouvert officiellement, configuration ChatonsInfos en cours, etc.)


   * service.registration.load : capacité à accueillir de nouveaux utilisateurs (obligatoire)
       * OPEN : le service accueille de nouveaux comptes
       * FULL : le service n'accueille plus de nouveau compte pour l'instant
       * ⚠️ modifier la liste des modalités d'accès pour qu'elle matche avec service.registration
   * service.registration : modalités d'accès au service
       * None : le service s'utilise sans inscription
       * Free : inscription ouverte à tout le monde et gratuite
       * Member : inscription restreinte aux membres (la notion de membre pouvant être très relative, par exemple, une famille, un cercle d’amis, adhérents d'association…).
       * Client : inscription liée à une relation commerciale (facture…).
       * ⚠️ Modifier le champ "Modalités d'accès" sur stats.chatons.org
           * fermé -> FULL
           * ouvert à toustes -> None
           * inscription obligatoire -> Free
           * cercle restreint -> Member
Note pour Angie : commencer  par ajouter champ service.registration.load avant de renommer les prop. du champ Modalités d'accès.



   * service.install.type -> type d'installation du service (obligatoire)
       * au choix parmi : 
       * DISTRIBUTION : installation via le gestionnaire d'une distribution (apt, yum, etc.).
       * PROVIDER : installation via le gestionnaire d'une distribution configuré avec une source externe (ex. /etc/apt/source.list.d/foo.list).
       * PACKAGE : installation manuelle d'un paquet compatible distribution (ex. dpkg -i foo.deb).
       * TOOLING : installation via un gestionnaire de paquets spécifique, différent de celui de la distribution (ex. pip…).
       * CLONEREPO : clone manuel d'un dépôt (git clone…).
       * ARCHIVE : application récupérée dans un tgz ou un zip ou un bzip2…
       * SOURCES : compilation manuelle à partir des sources de l'application.
       * CONTAINER : installation par containeur (Docker, Snap, Flatpak, etc.).
   * L'installation d'un service via un paquet Snap avec apt sous Ubuntu doit être renseigné CONTAINER.
   * L'installation d'une application ArchLinux doit être renseignée DISTRIBUTION.
   * L'installation d'une application Yunohost doit être renseignée DISTRIBUTION.




### Revue de [https://stats.chatons.org/](https://stats.chatons.org/) 

   * comment gérer les incohérences entre le statut déclaré et le statut technique ?
       * ajouter sur la page de chaque chaton un signe indiquant que le statut déclaré est contredit
       * TODO cpm
   * page uptime d'un chaton sorti du collectif
       * un chaton sorti continue d'être pingué, inutile
       * TODO Cpm filtrer les uptimes
   * **page générique d'un chaton** : 
       * penser à augmenter le code html avec les informations de properties pour faciliter le futur réagencement  UI/UX
           * TODO Cpm
   * **page « Statistiques » (fédération)** :
       * ajouter un donuts sur les services de paiement
           * TODO Cpm 
   * **un jour peut-être** :
       * pouvoir cliquer sur les graphiques pour voir la liste de résultats correspondant
           * par exemple pour les types d'inscription (à un service)
       * donuts sur les pays
           * pouvoir cliquer sur les résultats du camembert pour avoir une liste des chatons par pays
   * **pages Uptimes (Federation, Organization, Services)**
       * des améliorations à faire
           * TODO Cpm visibilité autres liens
           * TODO mrflos (pour l'été) : bidouiller la page statsuptime pour utiliser les filtres par état en js datatables
       * questions de statut manuel vs statut mesuré (page organization)
           * statut manuel seulement
           * statut mesuré seulement
           * les deux
           * un seul combiné des deux
           * discussion :
               * est-ce que la version manuelle est encore utile ? pertinence du mesuré
               * se poser la question de ce que cherche l'utilisateur
               * cas des statuts manuels « en travaux » ou  « fermé »
               * le statut manuel est plus important que le statut mesuré, respecté l'expression des admins
               * ne surtout pas afficher les deux
               * étudier la conjonction
               * garder l'un avec l'ajout d'un picto en plus si l'autres est KO
           * TODO Cpm voir pour une version « combinée » avec bulle informative
   * **Flo :**
       * peut être avoir dans résumé des moyennes sans graphes, genre du texte « sur 2020 XXX visiteurs uniques, YYY ips différentes »
           * Cpm : une notion de « tendance » ?
           * Cpm : donner exemple ?
           * TODO Flo, à réfléchir l'enrichissement de texte des graphes toujours en TODO  > GT le 20 juillet > décalé
           * TODO Flo, à réfléchir à des cadres de tendances dans « Résumé » toujours en  > GT le 20 juillet > décalé
       * changer les intitulés « Web » et « Spécifique » par « Graphes de visites web » ou plus court « Graphes Web » et « Statistiques propres aux services » ou plus court « Stats des services » ?
           * Cpm : préciser l'intention
           * Flo : expliquer les items du menu type > GT le 20 juillet (annulé cause covid...)  > décalé
           * TODO Cpm : ajouter des bulles
           * TODO Flo : tester le menu métriques auprès de personnes
               * en cours toujours en TODO
           * TODO réfléchir
       * TODO Cpm afficher les champs nom et description des métrics dans les diagrammes
   * **site statique vs site dynamique ?**
       * différence de besoin entre Libre-service.eu (métrique au jour) et le collectif CHATONS (mérique au mois)
       * supprimer les vues années, semaines et jours ?
       * supprimer les périodes 12 mois, 2020 et 2021 ?
       * site statique :
           * avantages : simplicité de déploiement (est-ce nécessaire ?)
           * inconvénients : prend de la place sur disque
       * pour site dynamique :
           * avantages : plus grande interactivité, plus de liberté fonctionnelles
           * inconvénients : nécessite l'installation d'un serveur d'application Java, du travail pour finaliser
       * avis :
           * Antoine : pas de problème au site dynamique
           * Flo : peut-être finir les fonctionnalités en cours avant de faire une version 2 dynamique avec des optimisations
   * **Test disponibilité en ipv6** :
       * [https://forum.chatons.org/t/suggestions-pour-la-version-0-6/3232](https://forum.chatons.org/t/suggestions-pour-la-version-0-6/3232)
       * est-ce une information déclarative ou à calculer ?
           * mieux si c'est calculé car plus à jour et un truc en moins à faire pour les admins
       * sujet non urgent, à approfondir
       * continuer à évaluer les besoins
       * page ipv6 pour avoir un aperçu du point de vue du collectif
   * **Vérifier la présence de trackers basic. **
       * [https://forum.chatons.org/t/suggestions-pour-la-version-0-6/3232](https://forum.chatons.org/t/suggestions-pour-la-version-0-6/3232)
       * oui, très pertinent
       * valorisation : à envisager page dédiée


### Revue générateur de fichiers métrics (statoolinfos)

   * ajout support Jitsi :
       * TODO Cpm en cours
   * ajout support Nextcloud
   * ajout support XMPP
   * metrics http
       * améliorer détection OS TODO Cpm
       * améliorer détection date TODO Cpm
   * pour info: mrflos travaille sur scrumblr et pourra faire la page stat
       * \o/
   * **App yunohost pour stats.chatons.org**
       * todo mrflos : contacter tobias a propos de l'appli yunohost chatons FAIT le 21 avril
       * ping (19/05/2022) FAIT 
       * réponse tobias
           * Salut,
           * J’ai complètement oublié de te répondre, j’ai presque rien fait, j’ai juste un début de python :
               * #! /usr/bin/python3
               * import sys
               * sys.path.insert(0, « /usr/lib/moulinette/ »)
               * import yunohost
               * yunohost.init\_logging()
               * yunohost.init\_i18n()
               * from yunohost.app import app\_info()from yunohost.app import app\_list()
               * apps = [ app[‹ id ›] for app in app\_list()[‹ apps ›] ]
           * Si vous organiser un repo, je voudrais bien le lien, je pourrais essayer de passer du temps dessus.
       * Créer un dépôt -> Angie le créé > FAIT (16/06)
           * [https://framagit.org/chatons/chatonsinfos-yunohost](https://framagit.org/chatons/chatonsinfos-yunohost)
           * trouver un animateur de ce dépôt (créer, surveiller, répondre etc..) -> voir avec Flo, Tobias ou Aleks
               * TODO CPM faire un appel sur le forum
           * TODO Angie : debrief  atelier camp
               * après relecture des CR d'ateliers, il s'avère que la question n'a pas été traitée lors du camp CHATONS. Relancer Flo, Tobias et Aleks ?
                   * OUI, il ne faut pas abandonner l'idée et cette liste de contact
                   * avec un « petit » cahier des charges, le futur contact sera plus efficace
                       * TODO CPM : rédiger un premier jet sur le pad proposé ci-dessous
           * idée de faire un cahier des charges (une simple liste de fonctionnalités)
               * pad à compléter : [https://mypads.framapad.org/p/cc-app-yunohost-stats-chatons-e4ikv7pj](https://mypads.framapad.org/p/cc-app-yunohost-stats-chatons-e4ikv7pj)
           * atelier pour faire un première échange oral
               * cpm, Aleks, Tobias 
               * Angie propose de caler un temps d'échange en début d'année 2023 pour discuter "faisabilité" 
                   * TODO Angie : envoyer un mp sur le forum FAIT
                   * pas encore de réponse concrête
                   * Angie en parle avec Tobias la semaine prochaine


### Revue des catégories

   * références :
       * [https://stats.chatons.org/category-autres.xhtml](https://stats.chatons.org/category-autres.xhtml)
       * [https://framagit.org/chatons/chatonsinfos/-/blob/master/StatoolInfos/categories/categories.properties](https://framagit.org/chatons/chatonsinfos/-/blob/master/StatoolInfos/categories/categories.properties)
   * autres :
       * docker
       * [https://stats.chatons.org/projetinternetetcitoyennete-hebergement.xhtml](https://stats.chatons.org/projetinternetetcitoyennete-hebergement.xhtml)
           * Hébergement de sites web, d'applications web en php, ou de conteneurs Docker
           * => hosting
           * TODO Angie : un MP sur le forum pour leur demander d'utiliser une fiche Hosting (en leur précisant qu'elle ne sera pas visible dans un premier temps)
       * enrichir la catégorie « Fournisseur de comptes Shell, machines physiques ou virtuelles » :
           * Docker, KVM, QEMU, libvirt, VirtualBox, Proxmox VE
           * TODO Angie FAIT




### Revue des fichiers properties de membres

   * passer en revue :
       *  [https://stats.chatons.org/chatons-crawl.xhtml](https://stats.chatons.org/chatons-crawl.xhtml)
           * plus tard
       * [https://stats.chatons.org/chatons-propertyalerts.xhtml](https://stats.chatons.org/chatons-propertyalerts.xhtml)
           * rouges : 527, ~~536~~, ~~527~~ ~~562~~ ~~581~~ ~~586~~ ~~580~~ ~~495~~ ~~500~~ ~~498~~ ~~284~~ ~~289 ~~ ~~292~~  ~~316~~
           * jaunes : 1278, ~~1305~~, ~~1258~~ ~~1266~~ ~~1273~~ ~~1292~~ ~~1287~~ ~~1211~~ ~~1208~~ ~~1209~~ ~~1220~~  ~~1217~~  ~~1220~~  ~~1226~~ 
           * communiquer un jour prochain (au moment de la prochaine release - celle avec les metrics)


### Revue des tickets

   * [https://framagit.org/chatons/chatonsinfos/-/issues/1](https://framagit.org/chatons/chatonsinfos/-/issues/1)
       * Redesign des encarts au dessus des tableaux
       * prévu lorsqu'on aura toutes les informations affichées
       * statut : plus tard
   * [https://framagit.org/chatons/chatonsinfos/-/issues/2](https://framagit.org/chatons/chatonsinfos/-/issues/2)
       * Dans le tableau des services de la fiche organisation des chatons, supprimer la colonne "Organisation"
       * Cpm : ce tableau est une vue mutualisée entre plusieurs pages : organisation, services, catégorie,  logiciel ; l'information est effectivement redondante pour la page organisation, mais ça permet de conserver l'homogénéité de la vue.
       * Cpm : pour gagner de la place, possibilité de ne mettre que le logo de l'organisation et le nom en bulle
       * statut : réfléchir et sinon sera traité par la grande revue visuelle prévue un jour
   * [https://framagit.org/chatons/chatonsinfos/-/issues/4](https://framagit.org/chatons/chatonsinfos/-/issues/4)
       * Penser un nouveau fichier properties dédié aux offres non logicielles, celle d'hébergement
       * statut : priorité aux services utilisateurs donc pertinent mais plus tard


### Revue des merge requests

   * [https://framagit.org/chatons/chatonsinfos/-/merge\_requests](https://framagit.org/chatons/chatonsinfos/-/merge\_requests)
   * [https://framagit.org/chatons/chatonsinfos/-/merge\_requests/49](https://framagit.org/chatons/chatonsinfos/-/merge\_requests/49)
       * Updated Ti Nuage organization URL
       * TODO Cpm FAIT
   * [https://framagit.org/chatons/chatonsinfos/-/merge\_requests/50](https://framagit.org/chatons/chatonsinfos/-/merge\_requests/50)
       * Modification de l'URL du fichier properties pour le-pic.org 
       * TODO Cpm FAIT
   * [https://framagit.org/chatons/chatonsinfos/-/merge\_requests/51](https://framagit.org/chatons/chatonsinfos/-/merge\_requests/51)
       * Ajout fichier properties de Ma Data de Défis 
       * TODO Cpm FAIT


### Revue du forum

   * [https://forum.chatons.org/c/collectif/stats-chatons-org/83](https://forum.chatons.org/c/collectif/stats-chatons-org/83)




### Revue des disponibilités des services (uptimes)

   * [https://stats.chatons.org/chatons-uptimes.xhtml](https://stats.chatons.org/chatons-uptimes.xhtml)
   * Tchat > Colibris
       * connu, en cours de réinstallation
       * toujours en cours de réflexion de réinstallation...
   * VPN / VPS Sans-nuage /ARN
       * ce sont des offres de services avec URL bien mise mais en 404
       * TODO Angie contacter FAIT
       * toujours rouge  pour VPS donc Angie refait un MP
       * retour à la normale
   * DevlogPro
       * pad, peertube, video
       * rouges depuis plus de 2 semaines
       * TODO Immae contacter -> contacté
           * travaux en cours
           * PT toujours en rouge
           * TODO Jeey : savoir ce qu'il en est -> FAIT (08/12)
               * Réponse (08/12)
               * En effet, je l'ai éteinte, faute d'utilité :)
               * Je vais la supprimer de la conf CHATONS.
           * pad et video OK
           * PeerTube tout rouge les 3 dernières semaines
           * TODO Immae relance 
       * retour à la normale
   * Wallabag de Kaihuri qui clignote
       * TODO Angie encourager à créer fiche CHATONS
       * 7 fiches services sur [https://stats.chatons.org/kaihuri.xhtml](https://stats.chatons.org/kaihuri.xhtml) à mettre sur chatons.org
       * recommandation de rediriger zoocoop.com vers kaihuri.org
       * retour à la normale


### Revue de l'ontologie

   * rappels :
       * l'ordre des questions à se poser : préfixe, sous-préfixe
       * on rajout .count si le mot signifiant est le préfixe
       * métriques HTTP :
           * contexte :
               * [https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/metrics.properties](https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/metrics.properties)
               * [http://www.webalizer.org/webalizer\_help.html](http://www.webalizer.org/webalizer\_help.html)
                   * [https://web.archive.org/web/20210330151216/http://www.webalizer.org/webalizer\_help.html](https://web.archive.org/web/20210330151216/http://www.webalizer.org/webalizer\_help.html)


   * mesure du CO2 :
       * en attente résultats du groupe de travail dédié
       * suite à la réunion du GT, l'élément le plus factuel serait la mesure de la consommation électrique (kWh) de chaque service. Sinon il n'y a pas de consensus sur les mesures.
       * cf un widget à la mode chez les lownumisateurices [https://www.websitecarbon.com/website/louisderrac-com-2021-11-03-eduquer-au-numerique-daccord-mais-pas-nimporte-lequel-et-pas-nimporte-comment/](https://www.websitecarbon.com/website/louisderrac-com-2021-11-03-eduquer-au-numerique-daccord-mais-pas-nimporte-lequel-et-pas-nimporte-comment/)
       * imaginer un système de grille pour agglomérer des indicateurs pour faire un indicateur
       * kwh et co2 ne sont pas suffisants, faut d'autres infos, âge machine, durée exploitation
       * certaines informations supplémentaires seraient donc nécessaires mais pas en tant que métrique mais fichier properties service


   * fiche properties hébergement
       * [https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/hosting.properties](https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/hosting.properties)
       * déjà 2 fichiers properties remplis !
       * projection d'intégration dans stats.chatons.org
           * besoin d'un bouton « Services dédiés » 
           * certainement renommer « Services » en « Services mutualisés » +1
           * page d'index : 
               * ajouter un nombre spécifique pour les services dédiés
               * ajouter un indicateur pour différencier
               * dans le tableau : un colonne pour les services dédiés / une pour les services mutualisés
           * page d'un membre :
               * ajouter un nombre spécifique pour les services dédiés
               * ajouter un indicateur pour différencier
           * nommage : service mutualisé vs service dédié
       * TODO Cpm intégrer dans le site stats.chatons.org
       * TODO communiquer auprès du collectif sur l'existence de cette nouvelle fiche


   * Ajout du champ organization.socialnetworks.pleroma
       * ajouté au fichier modèle organization.properties
       * TODO Cpm : implémenter StatoolInfos
       * pleroma = mastodon ? condisérer à part ? Ou introduire Fédivers ?
       * la demande initiale est de pouvoir valoriser les différentes communications des membres et donc avoir et le compte principale et les comptes secondaires, est pertinent


   * fiche service : champ service.website obligatoire
       * mais quand le service est la fourniture de comptes SSH (ou de DNS secondaires ou MixBackUp), il n'y a d'URL à donner et donc ça créé une erreur de remplissage
       * Solution : mettre l'URL d'une page web de présentation du service
       * FAIT angie (sur la fiche service.properties)  : 
           * *# Lien du site web du service (type URL, obligatoire - si pas possible, merci de créer une page de présentation du service).*
           * TODO Cpm propager aux autres modèles de service


   * prévoir de communiquer sur l'usage possible des stats
       * Mettre à jour [https://wiki.chatons.org/doku.php/aider/gt\_stats.chatons.org](https://wiki.chatons.org/doku.php/aider/gt\_stats.chatons.org) pour la prochaine réunion mensuelle (jeudi 15/12)
       * Demander aux participant⋅es pourquoi iels ne remplissent pas leurs fiches


### Épilogue

Date prochaine réunion ?

   * jeudi 15 juin
   * 11h15-13h00
   * sur [https://bbb.paquerette.eu/b/dom-67q-irl-nfc](https://bbb.paquerette.eu/b/dom-67q-irl-nfc)
