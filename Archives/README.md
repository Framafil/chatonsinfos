# Archives du groupe de travail pour stats.chatons.org

## Objectif du groupe de travail

Mettre en place un outil qui permettra de connaître le nombre d’utilisateur⋅ices des services proposés par chaque structure membre du collectif. La future page stats.chatons.org sera alimentée par des informations fournies par les structures membres du collectif.
* réfléchir à la solution la plus adaptée permettant d’alimenter la page stats.chatons.org
* identifier les données à récupérer auprès de chaque structure.

## Réunions Hebdomadaires

Toutes les semaines, les jeudis de 11h15 à 13h.

L'April propose d'utiliser leur serveur Mumble. Toutes les infos pour s'y connecter sur https://wiki.april.org/w/Mumble
Rendez-vous sur la terrasse Est . Merci de ne pas lancer l'enregistrement des réunions sans demander l'accord des participant⋅e⋅s.

## Comptes rendus

- [Groupes de travail 1 à 14 2020 gt-stats-2020.md](gt-stats-2020.md)
- [Groupes de travail 15 à 54 2021 gt-stats-2021.md](gt-stats-2021.md)
- [Groupes de travail 55 à 65 2022 gt-stats-2022.md](gt-stats-2022.md)
- [Groupes de travail 66 à 74 2023 gt-stats-2023.md](gt-stats-2023.md)

